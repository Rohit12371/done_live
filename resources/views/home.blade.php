@extends('layouts.app')

@section('content')
<div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box d-flex align-items-center justify-content-between">
                                    <h4 class="mb-0">Projects</h4>

                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                            <li class="breadcrumb-item active">Projects</li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                           
                        <?php foreach($project as $pro){ ?>
                            <div class="col-md-6 col-xl-3">
                                <div class="card project-list-box">                             
                                    <div class="card-body">                                       
                                        <div>
                                            <h4 class="mb-2 mt-0"><a href="{{route('list-task',[base64_encode($pro->id)])}}"><?php echo $pro->projectname; ?></a>
											 <a href="{{route('manage-task',[base64_encode($pro->id)])}}" class='add-new-task-link' data-bs-toggle="tooltip" data-bs-placement="top" title="Add New Task">Add New Task <i class="uil-plus-circle"></i></a>
											
											
											
											</h4>
                                            <?php $st = gettablerow("workflowstatuss",array("workflowsid"=>$pro->workflowname)); ?>
                                            <?php $prod = gettablerow("projecttasks",array("projectid"=>$pro->id)); ?>
                                           <?php $Stapre = array();
                                            foreach($prod as $pr) {
                                             $Stapre[] = $pr->workflowstatus;
                                            } ?>
                                           <?php foreach($st as $s){ ?>
                                               <?php if (in_array($s->id, $Stapre)) { ?>
                                               <?php $prodcount = gettablerow("projecttasks",array("workflowstatus"=>$s->id,"projectid"=>$pro->id)); ?>
                                               <a href ="{{route('list-task',[base64_encode($pro->id),base64_encode($s->id)])}}"><p class="text-muted mb-2"><?= $s->statusname; ?> <strong><?php echo count($prodcount); ?></strong></p></a>
                                            <?php } } ?>
                                            
                                        </div>                                       
                                      
                                    </div>
                                    
                                </div>
                            </div> <!-- end col-->
                            
                            <?php } ?>

                            

                          
                        </div> <!-- end row-->

                      
                    </div> <!-- container-fluid -->
                </div>
			
@endsection
