@extends('layouts.app')

@section('content')
<div class="page-content profile-container">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0"><i class='uil-cog'></i><span>Profile Settings for
                                    </span>Argos Help Desk </h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                        <li class="breadcrumb-item active">Edit Profile</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <hr>
                    <div class="row mt-4 mb-5">
                        <div class="col-md-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                                <a class="nav-link mb-2 active" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                                <a class="nav-link mb-2" id="v-pills-password-tab" data-bs-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false">Reset Password</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">

                                <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">

                                    <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                        <div class="card-header">
                                            <h3>Your ARGOS HELP DESK Profile</h3>
                                        </div>
                                        <div class="card-body">
                                            
                                             <div class="alert alert-success alert-dismissible fade hide" role="alert">
                                                <i class="uil uil-check me-2"></i>
                                               Profile updated successfully
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                                
                                                </button>
                                            </div>

                                            <form id="profile-form" name="updateprofile" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                              
                                                <div class="with-label">
                                                    <label for="name">Name</label>
                                                    <input id="name" name="profilename" type="text" value="<?php echo $userlogin[0]->name; ?>" tabindex="1" autofocus="autofocus" maxlength="100" placeholder="Name" required="" class="valid" aria-invalid="false">
                                                    <input name="profileupdateid" type="hidden" value="<?php echo $userlogin[0]->id; ?>"/>
                                                </div>
                                                
                                                <div class="with-label">
                                                    <label for="name">Email</label>
                                                    <input id="name" name="profileemail" type="text" value="<?php echo $userlogin[0]->email; ?>" tabindex="1" autofocus="autofocus" maxlength="50" placeholder="Email" required="" class="valid" aria-invalid="false" readonly>
                                                </div>
                                                
                                                <div class="with-label">
                                                    <label for="name">Phone</label>
                                                    <input id="name" name="profilephone" type="text" value="<?php echo $userlogin[0]->phone; ?>" tabindex="1" autofocus="autofocus" maxlength="20" placeholder="Phone" class="valid" aria-invalid="false">
                                                </div>
                                               
                                               <div>
                                                    <button type="submit" class='btn btn-primary waves-effect waves-light w-md' id="formpro" tabindex="5">Update my profile</button>
                                                </div>
                                            </form>

                                            <hr>

                                           
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                                        <div class="card-header"> 
                                            <h3>Reset Your Password</h3>
                                        </div>
                                        <div class="card-body">
                                             <div class="alert alert-success alert-dismissible fade hide" role="alert">
                                                <i class="uil uil-check me-2"></i>
                                               Password updated successfully
                                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                                
                                                </button>
                                            </div>

                                            <form method="post" name="passwordchange" action="#" novalidate="novalidate">
                                                <div>
                                                    <input type="password" id="oldpassword" name="oldpassword" placeholder="Old Password" autofocus="autofocus" tabindex="1" maxlength="50" required="">
                                                  <p class="passwordnotmatch" style="color:red; font-weight:600; display:none;">Password not match</p>
                                                </div>
                                                <div>
                                                    <input type="password" id="newpassword" name="newpassword" placeholder="New Password (Minlength = 8)" tabindex="2" maxlength="50" required="">
                                                     <input name="passwordupdateid" type="hidden" value="<?php echo $userlogin[0]->id; ?>"/>
                                                </div>
                                                <div class="right">
                                                    <button type="submit" class='btn btn-primary waves-effect waves-light w-md' id="formpass" tabindex="3">Reset Password</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Layout -->

                </div>

            </div>
            
    @endsection