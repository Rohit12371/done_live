@extends('layouts.app')

@section('content')
<style type='text/css'>
    body,body[data-layout-size=boxed] #layout-wrapper {    background-color: rgba(91, 140, 232, .25)!important;
        
}
main {
    min-height: 100vh!important;
}
</style>
<div class="account-pages my-5  pt-sm-5">
<div class="container">
<div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="/" class="mb-5 d-block auth-logo">
                               <h2><img src="../images/argos-helpdesk.png" alt=""></h2>
                            </a>
                        </div>
                    </div>
                </div>
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div class="card">
               

                <div class="card-body">
                <div class="text-center mt-2">
                                    <h5 class="text-primary">{{ __('Reset Password') }}</h5>
                                    <p class="text-muted">Reset Password with Argos Help Desk.</p>
                                </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12 text-end">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div></div>
@endsection
