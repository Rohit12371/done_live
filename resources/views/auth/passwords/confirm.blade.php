@extends('layouts.app')

@section('content')
<style type='text/css'>
    body,body[data-layout-size=boxed] #layout-wrapper {    background-color: rgba(91, 140, 232, .25)!important;
        
}
main {
    min-height: 100vh!important;
}
</style>
<div class="account-pages my-5  pt-sm-5">
<div class="container">
<div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="/" class="mb-5 d-block auth-logo">
                               <h2><img src="../images/argos-helpdesk.png" alt=""></h2>
                            </a>
                        </div>
                    </div>
                </div>
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div class="card">
               

                <div class="card-body">
                <div class="text-center mt-2">
                                    <h5 class="text-primary">{{ __('Confirm Password') }}</h5>
                                   
                                </div>
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div></div>
@endsection
