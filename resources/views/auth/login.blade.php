@extends('layouts.app')

@section('content')
<style type='text/css'>
    body,body[data-layout-size=boxed] #layout-wrapper {    background-color: rgba(91, 140, 232, .25)!important;
        
}
main {
    min-height: 100vh!important;
}
</style>
<div class="account-pages mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="/" class="mb-5 d-block auth-logo">
                               <h2><img src="images/argos-helpdesk.png" alt=""></h2>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">
                           
                            <div class="card-body p-4"> 
                                <div class="text-center mt-2">
                                    <h5 class="text-primary">Welcome Back !</h5>
                                    <p class="text-muted">Sign in to continue to Argos Help Desk.</p>
                                </div>
                                <div class="p-2 mt-4">
                                   <form method="POST" action="{{ route('login') }}">
                                    @csrf
        
                                        <div class="mb-3">
                                            <label class="form-label" for="email">{{ __('E-Mail Address') }}</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                
                                        <div class="mb-3">
                                            <div class="float-end">
                                                 @if (Route::has('password.request'))
                                                <a href="{{ route('password.request') }}" class="text-muted">{{ __('Forgot Your Password?') }}</a>
                                                 @endif
                                            </div>
                                        <label class="form-label" for="password">{{ __('Password') }}</label>
                                               <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>
                
                                        <div class="form-check">
                                             <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
                                        </div>
                                        
                                        <div class="mt-3 text-end">
                                            <button class="btn btn-primary w-sm waves-effect waves-light" type="submit">{{ __('Login') }}</button>
                                        </div>

                                        <!--<div class="mt-5 text-center">
                                            <p class="mb-0">Don't have an account ? <a href="{{ route('register') }}" class="fw-medium text-primary"> {{ __('Register') }} now </a> </p>
                                        </div>-->
            
                                    </form>
                                </div>
            
                            </div>
                        </div>

                        <div class="mt-5 text-center">
                            <p>© <script>document.write(new Date().getFullYear())</script> Argos Help Desk.</p>
                        </div>

                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
@endsection
