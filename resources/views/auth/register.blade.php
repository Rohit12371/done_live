@extends('layouts.app')

@section('content')
<style type='text/css'>
    body,body[data-layout-size=boxed] #layout-wrapper {    background-color: rgba(91, 140, 232, .25)!important;
        
}
main {
    min-height: 100vh!important;
}
</style>
<div class='account-pages my-5 pt-sm-5'>
<div class="container">
<div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a href="/" class="mb-5 d-block auth-logo">
                               <h2><img src="images/argos-helpdesk.png" alt=""></h2>
                            </a>
                        </div>
                    </div>
                </div>
    <div class="row align-items-center justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div class="card">
              

                <div class="card-body">
                <div class="text-center mt-2">
                                    <h5 class="text-primary">{{ __('Register') }} Account</h5>
                                    <p class="text-muted">Get your account now.</p>
                                </div>
                                <div class='p-2 mt-0 pt-0'>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-12 text-end">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <div class="mt-5 text-center">
                                            <p class="text-muted mb-0">Already have an account ? <a href="{{ route('login') }}" class="fw-medium text-primary"> {{ __('Login') }}</a></p>
                                        </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div></div>
@endsection
