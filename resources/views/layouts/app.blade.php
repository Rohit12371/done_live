<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Argos Help Desk</title>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}" defer></script>
    <script src="{{ asset('js/metisMenu.min.js') }}" defer></script>
    <script src="{{ asset('js/simplebar.min.js') }}" defer></script>
    <script src="{{ asset('js/waves.min.js') }}" defer></script>
    <script src="{{ asset('js/jquery.waypoints.min.js') }}" defer></script>
    <script src="{{ asset('js/validate/jquery.validate.min.js')}}" defer></script>
    <script src="{{ asset('js/validate/additional-methods.js')}}" defer></script>
    <script src="{{ asset('js/bootstrap-sweetalert/sweetalert.js')}}" ></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js')}}" defer></script>
    <script src="{{ asset('js/datepicker.min.js')}}" defer></script>
     <script src="{{ asset('js/bootstrap-tagsinput.js')}}" defer></script>
      <script src="{{ asset('js/bootstrap3-typeahead.js')}}" defer></script>
      
   
    <script src="{{ asset('js/appyyyyy.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     
      <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
      <link href="{{ asset('js/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
      <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
     
      
      <link href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal" data-topbar="colored" data-layout-size="boxed">
    <div id="layout-wrapper">
     @guest   
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm d-none">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- {{ config('app.name', 'Laravel') }} -->
                    Done Done
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

    @else
    
      <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">

                        <a href="{{route('home')}}" class="logo logo-light">
                            <h2 class='mt-3 text-white'><img src="{{ asset("/images/argos-helpdesk-white.png") }}" alt=""></h2>
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-16 d-lg-none header-item waves-effect waves-light" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>

                    <!-- App Search-->
                    <!--<form class="app-search d-none d-lg-block">
                        <div class="position-relative">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="uil-search"></span>
                        </div>
                    </form>-->
                </div>

                <div class="d-flex">

                    <div class="dropdown d-inline-block d-lg-none ms-2">
                        <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="uil-search"></i>
                            </button>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-search-dropdown">

                            <form class="p-3">
                                <div class="m-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>



                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="uil-user"></i>
                                <span class="d-none d-xl-inline-block ms-1 fw-medium font-size-15">{{ Auth::user()->name }}</span>
                                <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                            </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <!-- item-->
                            <a class="dropdown-item" href="{{route('profile')}}"><i class="uil-user"></i> <span class="align-middle">View Profile</span></a>
                            
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                   <i class="uil uil-sign-out-alt"></i> <span class="align-middle"> {{ __('Logout') }} </span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="topnav">

                    <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                        <div class="collapse navbar-collapse" id="topnav-menu-content">
                            <ul class="navbar-nav">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('home')}}">
                                        <i class="uil-home-alt me-2"></i> Home
                                    </a>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('list-task')}}" id="topnav-uielement">
                                        <i class="fa fa-pencil-square-o me-2" aria-hidden="true"></i>Task
                                    </a>


                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="{{route('report')}}" id="topnav-components">
                                        <i class="fa fa-file-text-o me-2" aria-hidden="true"></i>Reports
                                    </a>

                                </li>
                                
                                
                            <?php if(Auth::user()->id == 1){ ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('project')}}" id="topnav-more" role="button">
                                        <i class="uil-copy me-2"></i>Project/People
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{ url('workflow') }}" id="topnav-layout">
                                        <i class="uil-window-section me-2"></i>Work flow
                                    </a>
                                </li>
                                
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{ url('tasklog') }}" id="topnav-layout">
                                        <i class="fa fa-file-text-o me-2"></i>Task Logs
                                    </a>
                                </li>
                             <?php } else { ?> 
                             <?php $headerproject =  gettablerow("projects",array("projectstatus"=>1));
                              $checkadmin = array();
                              foreach($headerproject as $header){
                                  if($header->usertype1 =="Administrator"){
                                      if($header->notify1 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                  if($header->usertype2 =="Administrator"){
                                      if($header->notify2 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                  if($header->usertype3 =="Administrator"){
                                      if($header->notify3 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype4 =="Administrator"){
                                      if($header->notify4 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype5 =="Administrator"){
                                      if($header->notify5 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype6 =="Administrator"){
                                      if($header->notify6 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype7 =="Administrator"){
                                      if($header->notify7 == Auth::user()->id)
                                      { 
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype8 =="Administrator"){
                                      if($header->notify8 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype9 =="Administrator"){
                                      if($header->notify9 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                                  
                                   if($header->usertype10 =="Administrator"){
                                      if($header->notify10 == Auth::user()->id)
                                      {
                                         $checkadmin[]="present"; 
                                      }
                                  }
                              }?>
                              <?php if(!empty($checkadmin)){?>
                               <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('project')}}" id="topnav-more" role="button">
                                        <i class="uil-copy me-2"></i>Project/People
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{ url('workflow') }}" id="topnav-layout">
                                        <i class="uil-window-section me-2"></i>Work flow
                                    </a>
                                </li>
                              <?php } ?>
                             <?php } ?>
                          
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>

    @endguest


        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
