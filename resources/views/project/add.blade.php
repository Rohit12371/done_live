  @extends('layouts.app')  @section('content') 
    <div class="page-content workflow-add-container">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0">Add Projects</h4>
                                
                              

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                         <li class="breadcrumb-item"><a onclick="history.back()">Project List</a></li>
                                        <li class="breadcrumb-item active">Add Projects</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
<div class='row'>
		<div class='col-lg-12'>
		   <?php if(isset($project[0]->id)){ ?>
                        <div class="alert alert-success alert-dismissible fade hide" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Project updated successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                     <?php } else { ?>   
                         <div class="alert alert-success alert-dismissible fade hide" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Project created successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                        
                        <?php } ?>
		</div>
		</div>

                    <!-- end row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
<div class='row'>
					<div class='col-md-12'>
					<div class="mb-2"> <!--a href="{{route('manage-workflow')}}" class="btn btn-outline-primary waves-effect waves-light mb-3">Add Workflow</a-->
									<button type="button" class="btn btn-success waves-effect" data-bs-toggle="modal" data-bs-target="#myModal"> + Add More Company </button>
                                                        <br> <!--<a href="#" class="btn btn-outline-primary waves-effect waves-light mb-3">Project Workflow List</a>--> </div>
														<hr/>
					</div>
					</div>

                                    <div class="row">
                                        <div class="col-lg-12 mt-2">
                                            <div class="mt-0 mb-4 pb-2">
                                                <form name="projectform" method="post">
                                                     @csrf
                                                   
                                                    <div class="row">

                                                        <div class="col-md-5">
														 <div class="mb-3">
                                                        <label class="form-label mb-2" for="formrow-Fullname-input">Enter Company Name</label>
                                                        <div class="selectcom ">
                                                        <select name="companyname" class="form-control form-select">
                                                            <option value="">Select Company</option>
                                                            <?php foreach($company as $comp){?>
                                                            <option value="<?php echo $comp->id; ?>" <?php if(isset($project[0]->id) && $project[0]->companyname == $comp->id){echo 'selected';} ?>><?php echo $comp->companyname; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        </div>
                                                         <?php if(isset($project[0]->id)){ ?>
                                                            <input type="hidden" name="projectid" value="<?php echo $project[0]->id; ?>">
                                                         <?php } ?>
                                                    </div>
                                                    <div class="mb-3">
                                                        <label class="form-label" for="formrow-Fullname-input">Enter Project Name</label>
                                                        <input type="text" class="form-control" id="projectname" name="projectname" placeholder="Enter Project Name" value="<?php echo isset($project[0]->projectname)? $project[0]->projectname:''; ?>">
                                                    </div>
                                                    
                                                    <div class="mb-3">
                                                        <label class="form-label" for="formrow-Fullname-input">Enter Workflow</label> 
                                                         <select name="workflowname" class="form-control">
                                                            <option value="">Select Workflow</option>
                                                            <?php foreach($workflow as $work){?>
                                                            <option value="<?php echo $work->id; ?>" <?php if(isset($project[0]->id) && $project[0]->workflowname == $work->id){echo 'selected';} ?>><?php echo $work->workflow_name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        
                                                    </div>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h4 class="card-title mt-2 mb-3">Invite Clients Users</h4>
                                                                </div>
                                                                <?php if(isset($project[0]->id)){ ?>
                                                                <?php $inuse = explode(",",$project[0]->inviteclientusers); $tot = count($inuse); ?>
                                                                <?php $tr = 1; foreach( $inuse as $ins){
                                                                    if($ins != "!"){
                                                                    $usemail = gettablerow("users",array("id"=>$ins));
                                                                    $emailexist = $usemail[0]->email;
                                                                    }else{
                                                                     $emailexist = "";
                                                                    } ?>
                                                                <?php  if($tr == $tot){ ?>
                                                                <div class="col-md-12 renderhtmlproject">
                                                                    <div class="mb-3">
                                                                        <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                        <input type="email" class="form-control emailcheck" id="formrow-email-input" name="invite[]" maxlength="70" placeholder="Enter your email address" value="<?php echo $emailexist; ?>">
                                                                    </div>
                                                                </div>
                                                                <?php } else { ?>
                                                                 <div class="col-md-12">
                                                                    <div class="mb-3">
                                                                        <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                        <input type="email" class="form-control emailcheck" id="formrow-email-input" name="invite[]" maxlength="70" placeholder="Enter your email address" value="<?php echo $emailexist; ?>">
                                                                    </div>
                                                                </div>
                                                                <?php }
                                                                $tr ++; } ?>
                                                                
                                                                <?php } else { ?>
                                                                <div class="col-md-12">
                                                                    <div class="mb-3">
                                                                        <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                        <input type="email" class="form-control emailcheck" id="formrow-email-input" maxlength="70" name="invite[]" placeholder="Enter your email address">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="mb-3">
                                                                        <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                        <input type="email" class="form-control emailcheck" id="formrow-email-input" maxlength="70" name="invite[]" placeholder="Enter your email address">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 renderhtmlproject">
                                                                    <div class="mb-3">
                                                                        <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                        <input type="email" class="form-control emailcheck" id="formrow-email-input" name="invite[]" maxlength="70" placeholder="Enter your email address">
                                                                    </div>
                                                                </div>
                                                                <?php } ?>
                                                                <div class="col-md-12">
                                                                    <button type="button" class="btn btn-outline-secondary waves-effect addhtmlproject">
                + Add Another
            </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 ms-lg-auto">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h4 class="card-title mt-2 mb-3"> Notify Argos User's (Internal Users)</h4>
                                                                </div>
                                                            </div>
                                                                <div class="row notify1">
                                                                   <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                          
                                                                            <input type="text" list="browsers" maxlength="70" class="form-control emailcheck" id="formrow-email-input" name="notify1" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify1)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify1)); echo $usemailnot[0]->email; } ?>">
                                                                            <datalist id="browsers">
                                                                                <?php $r = 1; foreach($allusers as $all){ ?>
                                                                                <?php if($r > 0){ ?>
                                                                                 <option value="<?php echo ($all->name != 'invite user')? $all->email:$all->email; ?>">
                                                                                    <?php } $r++; } ?>
                                                                                
                                                                             </datalist>
                                                                         </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select" name="usertype1">
                                                                                <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype1) && $project[0]->usertype1 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype1) && $project[0]->usertype1 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row notify2">    
                                                                   <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                            <input type="text" list="browsers2" class="form-control emailcheck" maxlength="70" id="formrow-email-input" name="notify2" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify2)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify2)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email:$usemailnot[0]->email;} ?>">
                                                                            <datalist id="browsers2">
                                                                                <?php $e=1; foreach($allusers as $all){ ?>
                                                                                 <?php if($e > 0){ ?>
                                                                                 <option value="<?php echo ($all->name != 'invite user')? $all->email:$all->email; ?>">
                                                                                    <?php } $e++; } ?>
                                                                                
                                                                             </datalist>
                                                                        </div>
                                                                   </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select"  name="usertype2">
                                                                               <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype2) && $project[0]->usertype2 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype2) && $project[0]->usertype2 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                 
                                                                 <div class="row notify3" <?php if(isset($project[0]->id) && !empty($project[0]->notify3)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                   <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                            
                                                                            <input type="text" list="browsers3" class="form-control emailcheck" id="formrow-email-input" maxlength="70" name="notify3" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify3)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify3)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email; } ?>">
                                                                            <datalist id="browsers3">
                                                                                <?php $e=1; foreach($allusers as $all){ ?>
                                                                                 <?php if($e > 0){ ?>
                                                                                <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                    <?php } $e++; } ?>
                                                                                
                                                                             </datalist>
                                                                        </div>
                                                                   </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select"  name="usertype3">
                                                                               <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype3) && $project[0]->usertype3 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype3) && $project[0]->usertype3 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                 
                                                               <div class="row notify4" <?php if(isset($project[0]->id) && !empty($project[0]->notify4)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                <div class="col-md-8">
                                                                <div class="mb-3">
                                                                    <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                    
                                                                    <input type="text" list="browsers4" class="form-control emailcheck" maxlength="70" id="formrow-email-input" name="notify4" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify4)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify4)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email; } ?>">
                                                                    <datalist id="browsers4">
                                                                        <?php $e=1; foreach($allusers as $all){ ?>
                                                                         <?php if($e > 0){ ?>
                                                                        <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                            <?php } $e++; } ?>
                                                                        
                                                                     </datalist>
                                                                </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                <div class="mb-3">
                                                                    <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                    <select class="form-select"  name="usertype4">
                                                                       <option value="">Select</option>
                                                                        <option value="Administrator" <?php if(isset($project[0]->usertype4) && $project[0]->usertype4 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                        <option value="General User" <?php if(isset($project[0]->usertype4) && $project[0]->usertype4 == "General User"){echo 'selected';} ?>>General User</option>
                                                                    </select>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row notify5" <?php if(isset($project[0]->id) && !empty($project[0]->notify5)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                <div class="col-md-8">
                                                                <div class="mb-3">
                                                                    <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                    
                                                                    <input type="text" maxlength="70" list="browsers5" class="form-control emailcheck" id="formrow-email-input" name="notify5" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify5)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify5));  echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email; } ?>">
                                                                    <datalist id="browsers5">
                                                                        <?php $e=1; foreach($allusers as $all){ ?>
                                                                         <?php if($e > 0){ ?>
                                                                        <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                            <?php } $e++; } ?>
                                                                        
                                                                     </datalist>
                                                                </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                <div class="mb-3">
                                                                    <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                    <select class="form-select"  name="usertype5">
                                                                       <option value="">Select</option>
                                                                        <option value="Administrator" <?php if(isset($project[0]->usertype5) && $project[0]->usertype5 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                        <option value="General User" <?php if(isset($project[0]->usertype5) && $project[0]->usertype5 == "General User"){echo 'selected';} ?>>General User</option>
                                                                    </select>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                
                                                                <div class="row notify6" <?php if(isset($project[0]->id) && !empty($project[0]->notify6)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                    <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                            
                                                                            <input type="text" list="browsers6" class="form-control emailcheck" maxlength="70" id="formrow-email-input" name="notify6" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify6)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify6)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email; } ?>">
                                                                            <datalist id="browsers6">
                                                                                <?php $e=1; foreach($allusers as $all){ ?>
                                                                                 <?php if($e > 0){ ?>
                                                                                <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                    <?php } $e++; } ?>
                                                                                
                                                                             </datalist>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select"  name="usertype6">
                                                                               <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype6) && $project[0]->usertype6 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype6) && $project[0]->usertype6 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row notify7" <?php if(isset($project[0]->id) && !empty($project[0]->notify7)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                    <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                            
                                                                            <input type="text" list="browsers7" class="form-control emailcheck" id="formrow-email-input" maxlength="70" name="notify7" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify7)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify7)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email;  } ?>">
                                                                            <datalist id="browsers7">
                                                                                <?php $e=1; foreach($allusers as $all){ ?>
                                                                                 <?php if($e > 0){ ?>
                                                                                 <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                    <?php } $e++; } ?>
                                                                                
                                                                             </datalist>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select"  name="usertype7">
                                                                               <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype7) && $project[0]->usertype7 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype7) && $project[0]->usertype7 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row notify8" <?php if(isset($project[0]->id) && !empty($project[0]->notify8)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                    <div class="col-md-8">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                            
                                                                            <input type="text" list="browsers8" class="form-control emailcheck" id="formrow-email-input" maxlength="70" name="notify8" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify8)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify8)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email;  } ?>">
                                                                            <datalist id="browsers8">
                                                                                <?php $e=1; foreach($allusers as $all){ ?>
                                                                                 <?php if($e > 0){ ?>
                                                                                <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                    <?php } $e++; } ?>
                                                                                
                                                                             </datalist>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="mb-3">
                                                                            <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                            <select class="form-select"  name="usertype8">
                                                                               <option value="">Select</option>
                                                                                <option value="Administrator" <?php if(isset($project[0]->usertype8) && $project[0]->usertype8 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                <option value="General User" <?php if(isset($project[0]->usertype8) && $project[0]->usertype8 == "General User"){echo 'selected';} ?>>General User</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                    
                                                                    <div class="row notify9" <?php if(isset($project[0]->id) && !empty($project[0]->notify9)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                        <div class="col-md-8">
                                                                            <div class="mb-3">
                                                                                <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                                
                                                                                <input type="text" list="browsers9" class="form-control emailcheck"  maxlength="70" id="formrow-email-input" name="notify9" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify9)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify9)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email;  } ?>">
                                                                                <datalist id="browsers9">
                                                                                    <?php $e=1; foreach($allusers as $all){ ?>
                                                                                     <?php if($e > 0){ ?>
                                                                                    <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                        <?php } $e++; } ?>
                                                                                    
                                                                                 </datalist>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="mb-3">
                                                                                <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                                <select class="form-select"  name="usertype9">
                                                                                   <option value="">Select</option>
                                                                                    <option value="Administrator" <?php if(isset($project[0]->usertype9) && $project[0]->usertype9 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                    <option value="General User" <?php if(isset($project[0]->usertype9) && $project[0]->usertype9 == "General User"){echo 'selected';} ?>>General User</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        </div>
                                                                        
                                                                        <div class="row notify10" <?php if(isset($project[0]->id) && !empty($project[0]->notify10)){ ?> style="display:flex;" <?php } else { ?>style="display:none;" <?php } ?>>    
                                                                            <div class="col-md-8">
                                                                                <div class="mb-3">
                                                                                    <label class="form-label" for="formrow-email-input">Email Address</label>
                                                                                    
                                                                                    <input type="text" list="browsers10" class="form-control emailcheck" maxlength="70" id="formrow-email-input" name="notify10" placeholder="Enter your email address" value="<?php if(isset($project[0]->notify10)){ $usemailnot = gettablerow("users",array("id"=>$project[0]->notify10)); echo ($usemailnot[0]->name != 'invite user')? $usemailnot[0]->email." (".$usemailnot[0]->name.")":$usemailnot[0]->email; } ?>">
                                                                                    <datalist id="browsers10">
                                                                                        <?php $e=1; foreach($allusers as $all){ ?>
                                                                                         <?php if($e > 0){ ?>
                                                                                        <option value="<?php echo ($all->name != 'invite user')? $all->email." (".$all->name.")":$all->email; ?>">
                                                                                            <?php } $e++; } ?>
                                                                                        
                                                                                     </datalist>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="mb-3">
                                                                                    <label class="form-label" for="formrow-email-input">Select User Type</label>
                                                                                    <select class="form-select"  name="usertype10">
                                                                                       <option value="">Select</option>
                                                                                        <option value="Administrator" <?php if(isset($project[0]->usertype10) && $project[0]->usertype10 == "Administrator"){echo 'selected';} ?>>Administrator</option>
                                                                                        <option value="General User" <?php if(isset($project[0]->usertype10) && $project[0]->usertype10 == "General User"){echo 'selected';} ?>>General User</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                            
                                                            <div class="col-md-12">
                                                                    <button type="button" style="display:none;" class="btn btn-outline-secondary waves-effect addhtmlnotify" <?php if(isset($project[0]->id)){ ?>
                                                                    <?php if(isset($project[0]->notify10) && !empty($project[0]->notify10)){$roh = 10;}
                                                                     else if(isset($project[0]->notify9) && !empty($project[0]->notify9)){$roh = 9;}
                                                                     else if(isset($project[0]->notify8) && !empty($project[0]->notify8)){$roh = 8;}
                                                                     else if(isset($project[0]->notify7) && !empty($project[0]->notify7)){$roh = 7;}
                                                                     else if(isset($project[0]->notify6) && !empty($project[0]->notify6)){$roh = 6;}
                                                                     else if(isset($project[0]->notify5) && !empty($project[0]->notify5)){$roh = 5;}
                                                                     else if(isset($project[0]->notify4) && !empty($project[0]->notify4)){$roh = 4;}
                                                                     else if(isset($project[0]->notify3) && !empty($project[0]->notify3)){$roh = 3;}
                                                                     else if(isset($project[0]->notify2) && !empty($project[0]->notify2)){$roh = 2;} else if(isset($project[0]->notify1) && !empty($project[0]->notify1)){$roh = 1;} ?> data-show="<?php echo $roh; ?>" onclick="displayfunction(this)" <?php } else { ?> data-show="2" onclick="displayfunction(this)" <?php } ?>>  + Add Another </button>
                                                            </div>
                                                              
                                                            </div>
                                                        </div>


                                                    </div>

<hr/>


                                                    <div class="d-flex flex-wrap gap-3 mt-4">
                                                         <?php if(isset($project[0]->id)){ ?>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light w-md" id="formbut">Update Project</button>
                                                        <?php } else { ?>
                                                         <button type="submit" class="btn btn-primary waves-effect waves-light w-md" id="formbut">Create Project</button>
                                                        <?php } ?>
                                                        <button type="reset" class="btn btn-outline-danger waves-effect waves-light w-md resetdataall">Reset</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Layout -->




                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            
            <!-- Modal -->
<!-- sample modal content -->
                                                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel">Add Company</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form name="companynamepo" method="post">
                                                                    <input type="text" name="companynamepop" class="form-control"><br/>
                                                                    <button type="submit" class="btn btn-primary waves-effect waves-light popbutton">Save changes</button>
                                                                </form>
                                                                
                                                                    
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-light waves-effect" data-bs-dismiss="modal">Close</button>
                                                               
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            

            @endsection

