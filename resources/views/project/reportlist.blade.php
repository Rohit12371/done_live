@extends('layouts.app')

@section('content')

<div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                 <?php if($filter == 'show'){ ?>
                                <h4 class="mb-0">Report List </h4>
                                    <?php } ?>
                     
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                        <li class="breadcrumb-item active">Report List</li>
                                    </ol>
                                </div>
                                

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->


                    <!-- end row -->

                    <div class="row">
                        <div class="col-md-8">
                            <?php if($filter == 'show'){ ?>
                            <div id="search-filter">
                                <form name="reportform" method="post">
                                    <div class="row">
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <label class="form-label" >Select Project</label>
                                                <select name="selectproject" class="form-select">
                                                <option value="">Select project</option>
                                               <?php foreach($allproject as $allpro){ ?>
                                               <option value="<?= $allpro->id; ?>"><?= $allpro->projectname; ?></option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3 updatestatushtml">
                                                <label class="form-label" >Select Status</label>
                                                <select name="selectstatus" class="form-select selecthtmlclass">
                                                <option value="">Select status</option>
                                                
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <label class="form-label" >Select From Date</label>
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="selectfrom" class="form-control" placeholder="From Date"
                                                                data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                                data-date-autoclose="true">
                                                            <span class="input-group-text"><i class="uil-calendar-alt"></i></span>
                                                        </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <label class="form-label" >Select To Date</label>
                                                        <div class="input-group" id="datepicker2">
                                                            <input type="text" name="selectto" class="form-control" placeholder="To Date"
                                                                data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                                data-date-autoclose="true">
                                                            <span class="input-group-text"><i class="uil-calendar-alt"></i></span>
                                                        </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                       

                                    </div>
                                </form>
                            </div>
                            <?php } ?>
                        </div>
                       

                        <!--list view section start-->
                        <div class="col-lg-12 list-view-container" id="listViewContainer">
                            <div class="card">
                                <div class="card-body">
								<div class="table-responsive firstclass">
								</div>
                                   

                                </div>
                            </div>
                        </div>
                        <!--list view section end-->
                    </div>
                    <!-- End Form Layout -->




                </div>
                <!-- kanvan view start -->
               
                <!-- kanvan view end -->
            </div>
            <!-- End Page-content -->



@endsection


