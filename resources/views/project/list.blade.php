@extends('layouts.app')

@section('content')

 <div class="page-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="page-title-box d-flex align-items-center justify-content-between">
					<h4 class="mb-0">Project List</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
							<li class="breadcrumb-item active">Project List</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div  id='workflowList'>
			
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8 offset-md-2">
							<div class="mt-2"> <a href="{{route('manage-project')}}" class="btn btn-success waves-effect waves-light">Add Project</a>
										</div>
										<hr/>
								
										<div class="table-responsive">
											<table class="table table-bordered mb-0">
												<thead>
													<tr>
														<th>Project</th>
														<th>Company Name</th>
														<th>Created On</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													
													<?php foreach($list as $data) { ?>
													<tr>
													    <?php  $company = gettablerow('Companies',array("id"=>$data->companyname));  ?>
														<td><?= $data->projectname; ?></td>
														<td><?php echo $company[0]->companyname; ?></td>
														<td> <?php  $date = date_create($data->created_at); ?>
                                                               <?= date_format($date,"m/d/Y"); ?>
                                                        </td>
														<td>
														    <a href="{{route('manage-project',[base64_encode($data->id)])}}" class="btn btn-link waves-effect p-0"><i class='uil-pen'></i></a>
														    <?php  $projecttask = gettablerow('projecttasks',array("projectid"=>$data->id));
														    if(sizeof($projecttask) == 0){ ?> 
														    <a class="btn delete btn-link waves-effect p-0 sectiondeleteproject" delete-id="{{ $data->id }}" delete-table="projects"><i class='uil-trash-alt'></i></a>
														    <?php } ?>
														    <?php if($data->projectstatus == 1) { ?>
														    <a class="btn delete btn-link waves-effect p-0 sectiondisable" disable-id="{{ $data->id }}" disable-table="projects" field-status="projectstatus" status="0"><i class='uil-bell'></i></a>
														    <?php } else { ?>
														    <a class="btn delete btn-link waves-effect p-0 sectiondisable" disable-id="{{ $data->id }}" disable-table="projects"  field-status="projectstatus" status="1"><i class='uil-bell-slash'></i></a>
														    <?php } ?>
														</td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
											
											
										</div>
									</div>
								
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection