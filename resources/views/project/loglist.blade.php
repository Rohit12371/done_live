@extends('layouts.app')

@section('content')

 <div class="page-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="page-title-box d-flex align-items-center justify-content-between">
					<h4 class="mb-0">Task Log List</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
							<li class="breadcrumb-item active">Task Log List</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div  id='workflowList'>
			
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8 offset-md-2">
						
									
								
										<div class="table-responsive">
											<table class="table table-bordered mb-0">
												<thead>
													<tr>
														<th>User Name</th>
														<th>Action</th>
														<th>Description</th>
														<th>Created On</th>
														
													</tr>
												</thead>
												<tbody>
													
													<?php foreach($list as $data) { ?>
													<tr>
													    
														<td><?= $data->username; ?></td>
														<td><?= $data->action; ?></td>
														<td><?= $data->short_description; ?></td>
														<td> <?php  $date = date_create($data->created_at); ?>
                                                               <?= date_format($date,"m/d/Y"); ?>
                                                        </td>
													
													</tr>
													<?php } ?>
												</tbody>
											</table>
											
											
										</div>
									</div>
								
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection