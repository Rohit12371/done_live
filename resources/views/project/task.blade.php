@extends('layouts.app')

@section('content')

 <link href="{{ asset('css/basic.css') }}" rel="stylesheet" type="text/css" />
 
 
 
<div class="main-content">
     
   
      <div class="page-content task-with-editable">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                 <?php if(isset($taskrep[0]->id)){ ?>
                                <h4 class="mb-0">Edit Task <span class="badge bg-primary" style='font-size: 14px;vertical-align: middle;'><?php echo $project[0]->projectname; ?></span></h4>
                                <?php } else { ?>
                                <h4 class="mb-0">Add Task <span class="badge bg-primary" style='font-size: 14px;vertical-align: middle;'><?php echo $project[0]->projectname; ?></span></h4>
                                <?php } ?>
                                
                                <?php //echo "<pre>"; print_r($taskrep); echo "</pre>"; ?>
                                
                                 <?php if(isset($taskrep[0]->id)){ ?>
                        <div class="alert alert-success alert-dismissible fade hide m-0 taskalert" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Task updated successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                     <?php } else { ?>   
                         <div class="alert alert-success alert-dismissible fade hide m-0 taskalert" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Task created successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                        
                        <?php } ?>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                        <li class="breadcrumb-item"><a onclick="history.back()"><?php echo $project[0]->projectname; ?></a></li>
                                        <?php if(isset($taskrep[0]->id)){ ?>
                                        <li class="breadcrumb-item active">Edit Task</li>
                                        <?php } else { ?>
                                        <li class="breadcrumb-item active">Add Task</li>
                                        <?php } ?>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->


                    <!-- end row -->

                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="card">
                                <div class="card-body">


                                    <div class="row"> 
                                        <div class="col-lg-12">
                                            <div class="m-4 mb-0">
                                                <form name="taskform" method="post" class="dropzone" style='background: none;border: none;padding: 0;' id="my-awesome-dropzone" enctype="multipart/form-data">
                                                    <div class="mb-3 project-id">
                                                       <label class="form-label"><a onclick="history.back()" ><?php echo $project[0]->projectname; ?></a>&nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php if(isset($taskrep[0]->taskordernumber)){ ?> <?php echo $taskrep[0]->taskordernumber; ?> <?php } ?></label>
                                                        <input name="projectname" class="form-control" type="hidden" value="<?php echo $project[0]->projectname; ?>">
                                                        <input name="projectid" class="form-control" type="hidden" value="<?php echo $project[0]->id; ?>"> 
                                                    </div>
                                                    <div class="mb-3  project-title">
                                                        <!-- <label class="form-label" for="formrow-Fullname-input">Task Title</label> -->
                                                        <input type="text" name="tasktitle" class="form-control non-editable" id="formrow-Fullname-input" placeholder="Task title" value="<?php echo isset($taskrep[0]->tasktitle)? $taskrep[0]->tasktitle:''; ?>">
                                                         <?php if(isset($taskrep[0]->id)){ ?>
                                    <input type="hidden" name="taskid" value="<?php echo $taskrep[0]->id; ?>">
                                 <?php } ?>
                                                    </div>

                                                    <div class="row">
                                                                <div class="col-md-12">
																<div class="row">
																	<div class="col-md-4">
																	<div class="mb-3 custo-left-minus">
                                                                        <div class='view-container-global' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                        <label class="form-label" >Priority</label>
                                                                        <p><?php if(isset($taskrep[0]->id)){echo $taskrep[0]->priority;} ?></p>
                                                                        </div>
                                                                       <div class='edit-container-global'  <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                       <label class="form-label" >Select priority</label>
                                                                       <select name="priority" class="form-select">
                                                                            <option value="Low" <?php if(isset($taskrep[0]->id) && $taskrep[0]->priority == "Low"){echo 'selected';} ?>>Low</option>
                                                                            <option value="Medium" <?php if(isset($taskrep[0]->id) && $taskrep[0]->priority == "Medium"){echo 'selected';} ?>>Medium</option>
                                                                            <option value="High" <?php if(isset($taskrep[0]->id) && $taskrep[0]->priority == "High"){echo 'selected';} ?>>High</option>
                                                                        </select>
                                                                       </div> 
                                                                    </div>
																	</div>
																	<div class="col-md-4">
																	<div class="mb-3 custo-left-minus">
                                                                    <div class='view-container-global' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                        <label class="form-label" >Work flow status</label>
                                                                        <?php if(isset($taskrep[0]->id)){?>
                                                                        <p><?php $stashow = gettablerow("workflowstatuss",array("id"=>$taskrep[0]->workflowstatus)); if(sizeof($stashow)>0){ echo $stashow[0]->statusname; } ?></p>
                                                                        <?php } ?>
                                                                        </div>
                                                                        <div class='edit-container-global'  <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                        <?php if($utype == 2){ ?> 
                                                                         <label class="form-label" > Work flow status</label>
                                                                        <input type="text" class="form-control" name="showcustomer" value="<?php echo $workflowstatus[0]->statusname; ?>" readonly>
                                                                        <input type="hidden" name="workflowstatus" value="<?php echo $workflowstatus[0]->id; ?>">
                                                                        <?php } else { ?>
                                                                        <label class="form-label" >Select Work flow status</label>
                                                                        <select name="workflowstatus" class="form-select">
                                                                            <?php foreach($workflowstatus as $workflow){?>
                                                                            <option value="<?php echo $workflow->id; ?>" <?php if(isset($taskrep[0]->id) && $taskrep[0]->workflowstatus == $workflow->id){echo 'selected';} ?>><?php echo $workflow->statusname; ?></option>
                                                                            <?php }?>
                                                                           
                                                                        </select>
                                                                        <?php } ?>
                                                                        
                                                                    </div></div>
																	</div>
																	 <?php if($utype != 2){ ?> 
																	<div class="col-md-4">
																	<div class='date-view' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                    <label class="form-label" >Date</label>    
                                                                    <p><?php echo isset($taskrep[0]->duedate)? $taskrep[0]->duedate:''; ?></p></div>
                                                                    <div class='date-editor' <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                    <label class="form-label" >Select Date</label>
                                                                    <div class="input-group" id="datepicker2">
                                                            <input type="text" name="dueto" class="form-control" placeholder="Due Date"
                                                                data-date-format="dd M, yyyy" data-date-container='#datepicker2' data-provide="datepicker"
                                                                data-date-autoclose="true" value="<?php echo isset($taskrep[0]->duedate)? $taskrep[0]->duedate:''; ?>">
        
                                                            <span class="input-group-text"><i class="uil-calendar-alt"></i></span>
                                                        </div><!-- input-group -->
                                                                    </div>
																	</div>
																	<?php } ?>
																</div>
																</div>
                                                                <div class="col-md-12">
																<h4 class="card-title mt-2 mb-3">Assign To</h4>
																</div>
																 <div class="col-md-12">
																<div class="row">
																	<div class="col-md-4">
																	<div class="mb-3 custo-left-minus">
                                                                    <div class='view-container-global' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                        <label class="form-label" >User</label>
                                                                        <p><?php if(isset($taskrep[0]->id) && !empty($taskrep[0]->assignto)){$usemailnot = gettablerow("users",array("id"=>$taskrep[0]->assignto)); echo $usemailnot[0]->email." (".$usemailnot[0]->name.")"; } ?></p>
                                                                        </div>
                                                                        <div class='edit-container-global'  <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                        <label class="form-label" >Select User</label>
                                                                        <select name="assignto" class="form-select">
                                                                            
                                                                            <?php if($project[0]->usertype1 == "Administrator"){ ?>
                                                                            <option value="<?php echo $project[0]->notify1; ?>" <?php if(isset($taskrep[0]->id) && $taskrep[0]->assignto == $project[0]->notify1){echo 'selected';} ?>> <?php $usemailnot = gettablerow("users",array("id"=>$project[0]->notify1)); echo $usemailnot[0]->email." (".$usemailnot[0]->name,")"; ?> </option>
                                                                            <?php } ?>
                                                                             <?php if($project[0]->usertype2 == "Administrator"){ ?>
                                                                            <option value="<?php echo $project[0]->notify2; ?>" <?php if(isset($taskrep[0]->id) && $taskrep[0]->assignto == $project[0]->notify2){echo 'selected';} ?>> <?php $usemailnot = gettablerow("users",array("id"=>$project[0]->notify2)); echo $usemailnot[0]->email." (".$usemailnot[0]->name,")"; ?> </option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    </div>
																	</div>
																	<div class="col-md-4">
																	<div class="mb-3 custo-left-minus">
                                                                    <div class='view-container-global' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                        <label class="form-label" >Tags</label>
                                                                        <p><?php echo isset($taskrep[0]->tags)? $taskrep[0]->tags:''; ?></p>
                                                                        </div>
                                                                        <div class='edit-container-global'  <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                        <label class="form-label" >Select Tags</label>
                                                                       <input id="title" name="tags" data-role="tagsinput" size="8" type="text" class="input-large form-control" placeholder="Add tags" value="<?php echo isset($taskrep[0]->tags)? $taskrep[0]->tags:''; ?>">
                                                                             </div>
                                                                    </div>
																	</div>
																	
																
																</div>
																</div>
															<!--	<div class="col-md-12 mb-3">
                                                                    <button type="button" class="btn btn-outline-secondary waves-effect">
																	+ Add Another
																</button>
                                                                </div>-->
																<div class="col-md-12 mb-0 delhiassign">
																
                                                                <div class='message-view-container' <?php if(!isset($taskrep[0]->id)){ ?> style='display:none;' <?php } ?>>
                                                                <label class="form-label"  for="">Description</label>
                                                                <p><?php echo isset($taskrep[0]->description)? $taskrep[0]->description:''; ?></p>
                                                                </div>
                                                                <div class="attachement-list">
                                                                    <ul>
                                                                    <?php if(isset($taskrep[0]->id)){ ?>
                                                <?php $images = gettablerow("projecttask_images",array("recognizetext" => $taskrep[0]->id)); ?>
                                                                    <?php foreach($images as $imgLink){ ?>  
                                                                        <li>
                                                                            <a href='{{ asset($imgLink->image) }}' target='_blank'><i class="uil-image"></i>&nbsp;  <?= str_replace("product_image/","",$imgLink->image);?></a>
                                                                        </li>
                                                                    <? } }?>
                                                                    </ul>
                                                                </div>
                                                                 <div class='message-edit-container' <?php if(isset($taskrep[0]->id)){ ?> style='display:none;' <?php } else { ?> style='display:block;' <?php } ?>>
                                                                 <label class="form-label"  for="">Write a Description</label>
                                                                 <textarea name="editor1" class="non-editable" id="editor1" rows="10" cols="80"><?php echo isset($taskrep[0]->description)? $taskrep[0]->description:''; ?></textarea>
                                                                <div class="row">
                                                                <div class="col-md-12 mb-0 mt-4 custom-file-upload"> 
                                                   	<label class="form-label"  for="">Upload file <span class='red'>(Please upload jpg,png,pdf,doc file only)</span></label>
													
                                                  
                                                 
                                                   </div> 
												   
												   <div class="col-md-12 mb-3 mt-0 custom-file-upload">
												  
                                               
                                                <div id="myId" class="dropzone_" style="width:100%;height:120px;border: 1px dotted #cccccc;">
                                               
                                                  <div class="fallback">
                                                    <input name="file" type="file" multiple  style="display:none;"/>													
                                                  </div>
	
                                                </div>
												 <div class="cust-dz-message cust-needsclick">
<div class="mb-0">
<i class="display-4 text-muted uil uil-cloud-upload"></i>
</div>

<h4>Drop files here or click to upload.</h4>
</div>

 
												   </div>
												   
												   <div class='col-md-12 mb-0 mt-0 custom-file-upload'>
												   <div id="dropzone-previews">
                                                  </div>
<?php if(isset($taskrep[0]->id)){ ?>
                                                <?php $images = gettablerow("projecttask_images",array("recognizetext" => $taskrep[0]->id)); ?>
                                                <div class='image-preview'>
												<?php foreach($images as $img){ ?>            <?php $ext = explode('.', $img->image); 			$image_ext = array('jpeg','png','jpg');			if(in_array($ext[1],$image_ext)){			?>
            <span class="<?php echo $img->id;?>">          
            <a href='javascript:void(0)'>  <img src="{{ asset($img->image) }}" id="blah" style=""> <span class="badge" delete-id="<?php echo $img->id;?>" onclick="deleteimage(this)" style="margin: 5px 10px 89px -18px;">x</span> </a>
            </span>			<?php } else { ?>			<span class="<?php echo $img->id;?>">                      <a href='javascript:void(0)'>  <img src="https://helpdesk.argosinfotech.net/images/argos-helpdesk.png" id="blah" style=""> <span class="badge" delete-id="<?php echo $img->id;?>" onclick="deleteimage(this)" style="margin: 5px 10px 89px -18px;">x</span> </a>            </span>			<?php } ?>
                <?php } } ?></div>
												   </div>
                                                                </div>
                                                                </div>
                                                                </div>
                                                                
                                                          
                                                                
                                                           


                                                    
                                                         <?php if(isset($taskrep[0]->id)){ ?>
														 <div class="d-flex flex-wrap gap-3 ">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light w-md import updateimport mt-3 mb-3" style="display:none;">Update Task</button>
														 </div>
                                                        <?php } else { ?>
														 <div class="d-flex flex-wrap gap-3 mt-3 mb-3">
                                                         <button type="submit" class="btn btn-primary waves-effect waves-light w-md import">Create Task</button>
                                                         <button type="submit" class="btn btn-primary waves-effect waves-light w-md resetdataall">Reset</button>
														  </div>
                                                        <?php } ?>
                                                        
                                                        
                                                   
                                                </form>
                                                
                                               </div>
                                                
                                                
                                                
                                            </div>
                                        </div>

                                    </div>
                               </div>
                            </div>
                            
                             <?php if(isset($taskrep[0]->id)){ ?> 
                            
                        <div class="card comment-container">
                            <div class="card-body">
                              <div class="row">
                                    <div class="col-lg-12">
                                        <div class='m-4 mt-0'>
                                        <div class="alert alert-success alert-dismissible fade hide m-0 comalert" role="alert">
                                            <i class="uil uil-check me-2"></i>
                                           Comment posted successfully
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                                            
                                            </button>
                                        </div>
                                       
                                        <div class="commentpreiview" mode="in-out" <?php if(sizeof($taskcomment) == 0){?> style="display:none;" <?php } ?>>
                                            <?php foreach($taskcomment as $tas){ ?>
                                            <?php  $udet = getuserdetail($tas->user_id);
                                           $date=date_create($tas->created_at);
                                           $pdate =  date_format($date,"m/d/Y"); ?>
                                            <div id="e2281051" class="chronology__item chronology__item--action">
                                                <div class="chronology-action">
                                                    <div class="chronology-action__avatar">
                                                        <div title="Mark Gorman- Spirit Worx" class="avatar avatar--light-orange" style="width: 40px; height: 40px; border-radius: 50%;"><span class='avtar-first-letter'><?php $first_char = mb_substr($udet[0]->name, 0, 1); echo $first_char; ?></span>
                                                    </div>
                                                </div>
                                                <div class="chronology-action__date"><?= $pdate; ?></div>
                                                <div class="chronology-action__action-text"><strong><?= $udet[0]->name ?></strong> <?= $tas->comments ; ?>
                                              </div></div></div>
                                          <?php } ?>
                                      </div>
                                       
                                       <div class="mt-0">
                                           <form name="commentform" method="post" enctype="multipart/form-data">
                                              
                                              	<div class="col-md-12 mb-3">
												<label class="form-label"  for=""> Comment </label>
                                                <div class='add-comment-view'>
                                                    <p>Leave a comment...</p>
                                                </div>
                                                  <div class='editable-comment'  style='display:none;'>
                                                  <textarea name="editor2" class="non-editable" id="editor2" rows="10" cols="80"> </textarea>
                                                  <input type="hidden" name="commenttaskid" value="<?= $taskrep[0]->id; ?>"/>
                                                   <input name="commentprojectid" type="hidden" value="<?php echo $project[0]->id; ?>">
                                                  <input type="hidden" name="userid" value="{{ Auth::user()->id }}"/>
                                                  <input type="hidden" name="commenttasktitle" value="<?php echo isset($taskrep[0]->tasktitle)? $taskrep[0]->tasktitle:''; ?>">
                                                  <input type="hidden" name="projectname" value="<?php echo isset($project[0]->projectname)? $project[0]->projectname:''; ?>">
                                                  
                                                  <div class='extra-comment-option'>
                                                  <div class="col-md-12 ">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="">
                                                                        <label class="form-label">Current Status</label>
                                                                        <?php $wosta =  gettablerow("workflowstatuss",array("id"=>$taskrep[0]->workflowstatus)); ?>
                                                                        <input name="currentstatus" type="hidden" value="<?= $taskrep[0]->workflowstatus; ?>" />
                                                                        <input name="currentstatusname" class="form-control" value="<?php if(sizeof($wosta)>0) { echo $wosta[0]->statusname;} ?>" readonly />
                                                                         
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="">
                                                                        <label class="form-label">Update Status to</label>
                                                                       <select name="updatestatus" class="form-select">
                                                                           <option value="">Select Status</option>
                                                                            <?php foreach($workflowstatus as $workflow){?>
                                                                            <option value="<?php echo $workflow->id; ?>"><?php echo $workflow->statusname; ?></option>
                                                                            <?php }?>
                                                                           
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                <div class="comment-or-reply__body-submit">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light w-md import2">Add Comment</button></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                  </div>
                                                </div>
                                                </div>
                                              
                                           </form>
                                      </div>
                                        </div>
                                   </div>
                                </div>
                              </div>
                          </div>
                          
                          <?php } ?>
                            
                            
                        </div>
                    </div>  
                    <!-- End Form Layout -->




                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            
           
		    <script src="{{ asset('js/dropzone.js') }}"></script>
			<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
           
            <script>
			var basic = [
                      ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-','Image', 'PageBreak', 'NewPage', 'Styles', 'Format']
                    ];
                    
                    var standard = [{
                        name: 'document',
                        items: ['NewPage', 'Preview']
                      }, {
                        name: 'clipboard',
                        items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                      }, {
                        name: 'editing',
                        items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                      }, {
                        name: 'insert',
                        items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']
                      },
                      '/', {
                        name: 'styles',
                        items: ['Styles', 'Format']
                      }, {
                        name: 'basicstyles',
                        items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
                      }, {
                        name: 'paragraph',
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
                      }, {
                        name: 'links',
                        items: ['Link', 'Unlink', 'Anchor']
                      }, {
                        name: 'tools',
                        items: ['Maximize', '-', 'About']
                      }
                    ];
             CKEDITOR.replace('editor1', {
				    toolbar: basic,
					filebrowserUploadUrl: "{{route('ckimage', ['_token' => csrf_token() ])}}",
					filebrowserUploadMethod: 'form'
				});
			 CKEDITOR.replace('editor2', {
				    toolbar: basic,
					filebrowserUploadUrl: "{{route('ckimage', ['_token' => csrf_token() ])}}",
					filebrowserUploadMethod: 'form'
				});	
             
            </script>
 @endsection           
            