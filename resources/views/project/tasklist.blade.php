@extends('layouts.app')

@section('content')

<div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                 <?php if($filter == 'show'){ ?>
                                <h4 class="mb-0">Task List:
                                    <div class="dropdown d-inline-block">
                                        <a class="dropdown-toggle text-reset" href="#" id="dropdownMenuButton5" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="fw-semibold"></span> <span class="text-muted" taka-value="">All<i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton5">
                                            <a class="dropdown-item" onclick="getfilter(this)" taka-value="" pick-value="All"><i class='uil-check'></i> All</a>
                                            <?php foreach($allproject as $allpro){ ?>
                                            <a class="dropdown-item" onclick="getfilter(this)" taka-value="<?= $allpro->id; ?>" pick-value="<?= $allpro->projectname; ?>"> <?= $allpro->projectname; ?></a>
                                           <?php } ?>
                                        </div>
                                    </div> </h4>
                                    <?php } ?>
                     
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                        <li class="breadcrumb-item active">Task List</li>
                                    </ol>
                                </div>
                                

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->


                    <!-- end row -->

                    <div class="row">
                        <div class="col-md-8">
                            <?php if($filter == 'show'){ ?>
                            <div id="search-filter">
                                <form>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <input id="title" name="filtertitle" type="text" class="input-large form-control" onkeyup="myCommonfilter(this)" placeholder="Filter by title">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <select name="selectfilterstatus" class="form-select" onchange="myCommonfilter(this)">
                                                <option value="">Select status</option>
                                                <?php foreach($getsearchstatus as $allass){ ?>
                                                <?php if($user == 1){ ?>
                                                  <option value="<?= $allass->id; ?>"><?php $Stacount = gettablerowspecific("projecttasks",array("workflowstatus"=>$allass->id),'id'); ?><?= $allass->statusname; ?> (<?= count($Stacount); ?>)</option>
                                                <?php } else { ?>
                                                 <option value="<?= $allass->id; ?>"><?php $Stacount = gettablerowspecificuser("projecttasks",array("workflowstatus"=>$allass->id),'id',$user); ?><?= $allass->statusname; ?> (<?= count($Stacount); ?>)</option>
                                                <?php } ?>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="mb-3">
                                                <select name="selectfilterpriority" class="form-select" onchange="myCommonfilter(this)">
                                                <option value="">Select priority</option>
                                                <option value="High">High</option>
                                                <option value="Medium">Medium</option>
                                                <option value="Low">Low</option>
                                               
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-3">
                                            <div class="mb-3">
                                                <select name="filterassign" class="form-select" onchange="myCommonfilter(this)">
                                                <option value="">Assigned To</option>
                                                <?php foreach($allassign as $allass){ ?>
                                                <option value="<?= $allass->assignto; ?>"><?php $usemail = gettablerow("users",array("id"=>$allass->assignto)); ?><?= $usemail[0]->email; ?> (<?= $usemail[0]->name; ?>)</option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>-->
                                        <!--<div class="col-md-3">
                                            <div class="mb-3">
                                                <select name="sortby" class="form-select" onchange="myCommonfilter(this)">
                                                <option value="">Sort: Updated</option>
                                                <option value="ASC">Ascending order</option>
                                                <option value="DESC">Decending order</option>
                                                </select>
                                            </div>
                                        </div>-->

                                    </div>
                                </form>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-4 text-end mb-3">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="javascript:void(0);" id='listView' class="btn btn-outline-primary active" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="List View" aria-current="page"><i class="uil-list-ul"></i></a>
                                <a href="javascript:void(0);" id='kanvanView' class="btn btn-outline-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Kanban View"><i class="uil-apps"></i></a>
                            </div>
                        </div>

                        <!--list view section start-->
                        <div class="col-lg-12 list-view-container" id="listViewContainer">
                            <div class="card">
                                <div class="card-body">
                                    <form>

                                        <div class="table-responsive firstclass">
                                            <table id="datatable" class="table table-bordered dt-responsive nowrap" style=" width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th  colspan="6">Task</th>
                                                       
                                                    </tr>
                                                </thead>


                                                <tbody>
                                                    
                                                    <?php foreach($alltask as $allta) { ?>
                                                    <tr>
                                                        <td class='col-status'><a href="{{route('manage-task',[base64_encode($allta->projectid),base64_encode($allta->id)])}}"><span class="badge bg-warning"  style="background-color: <?php echo $allta->color; ?> !important;"> <?= $allta->status_name; ?> </span><a></td>
                                                            <td class='col--item-info three-lines' colspan='3'>
                                                            <div class="line line-1"><a href="{{route('manage-task',[base64_encode($allta->projectid),base64_encode($allta->id)])}}" class="just-a-link">
                                                            <?= $allta->project_name; ?>
                                                            </a></div>
                                                            <a href="{{route('manage-task',[base64_encode($allta->projectid),base64_encode($allta->id)])}}">
                                                                <div class="line line-1"><span class="item-title"><?= $allta->taskordernumber; ?>: <?= $allta->tasktitle; ?></span></div>
                                                                <div class="line line-2"><span class="item-desc"><?= substr($allta->description,0,150); ?></span></div>
																<!--<div class="line line-3"><span title="Updated." class="item-desc">Updated.</span></div>-->
                                                            </a>
                                                            </a>
                                                           </td>
                                                          
                                                          <td style='width:15%;'><span title="Thread count"><?php if($allta->commentscounts > 0){?><i class="uil-comment"></i><span><?= $allta->commentscounts; ?></span><?php } ?></span> 
														  <?php if($allta->priority == "High") { ?>
                                                     <!--priority-->
														  <div title="High" class="chicklet--small " style='background-color: #fa6123;  border: 1px solid #fa6123;'>
														  <span class="chicklet__content">
														     
														  <img src="{{ asset("/images/high.svg") }}">
														  </span>
														  </div>
													 <!--priority end-->
                                                    <?php } ?>
                                                    <?php if($allta->priority == "Medium") { ?>
                                                    <!--priority-->
														  <div title="Medium" class="chicklet--small " style='background-color: #faa33e; border: 1px solid #faa33e;'>
														  <span class="chicklet__content">
														  <img src="{{ asset("/images/medium.svg") }}">
														  </span>
														  </div>
													<!--priority end-->
                                                    <?php } ?>
                                                    <?php if($allta->priority == "Low") { ?>
                                                    <!--priority-->
														  <div title="Low" class="chicklet--small " style='background-color: #198c62; border: 1px solid #198c62;'>
														  <span class="chicklet__content">
														  <img src="{{ asset("/images/low.svg") }}">
														  </span>
														  </div>
													<!--priority end-->
                                                    <?php } ?>
														<!--avatar start-->
														  <!--<div title="Argos Help Desk" class="avatar avatar--algae-green"  tabindex="0" ><span style="font-size: 16px; font-weight: 500; color: rgb(255, 255, 255);">A</span></div>-->
														  <!--avatar start end-->
														  </td>
                                                          <td class='col-last-updated'><?php $date = date_create($allta->created_at);
                                                                  echo date_format($date,"m/d/Y");  ?></td>
                                                    </tr>
                                                    
                                                    <?php } ?>
                                                   
                                                 </tbody>
                                            </table>
                                        </div>


                                    </form>

                                </div>
                            </div>
                        </div>
                        <!--list view section end-->
                    </div>
                    <!-- End Form Layout -->




                </div>
                <!-- kanvan view start -->
                <div id="KanviViewContainer" class='row mt-3' style='display: none;'>
                    <div class="kanban-board__columns  secondclass">
                          <?php
                          $pre = array();
                          foreach($allstatus as $stat){
                             foreach($alltask as $allta1) {
                                  if($allta1->workflowstatus == $stat->id)
                                 {
                                    $pre[] = $allta1->workflowstatus;
                                 }
                             } 
                           }
                       ?>
                          <?php foreach($allstatus as $workr){
                             
                          if(in_array($workr->id, $pre)){ 
                          $taskby = getAllTaskListbystatus($workr->id,$taskproject);
                          ?>
                          
                         
                  
                        <div class="kanban-column">
                            <div class="card kanvan-view-list-container" >
                               
                                <div class="card-body">
                         <?php $rsta = gettablerow("workflowstatuss",array("id"=>$workr->id)); ?>
                                    <h4 class="card-title mb-4 bg-primary" style="background-color:<?php echo $rsta[0]->color; ?> !important;"><?php  echo $rsta[0]->statusname; ?> </h4>

                                    <div class="kanban-column__card-holder">
                                        <ul>
                                             <?php foreach($taskby as $task){ ?>
                                            <li class="" >
                                                <a href="{{route('manage-task',[base64_encode($task->projectid),base64_encode($task->id)])}}"><p><?= $task->project_name; ?></p>
                                                <h4><?= $task->tasktitle; ?></h4></a>
                                                <div>
                                                    <!---->
                                                    <?php if($task->priority == "High") { ?>
                                                    <div title="High" style='background-color: #fa6123;  border: 1px solid #fa6123;' class="chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase"><span class="chicklet__content"><span><img src="{{ asset("/images/high.svg") }}"><span class="with-icon"><?= $task->priority; ?></span></span>
                                                        </span>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($task->priority == "Medium") { ?>
                                                    <div title="Medium" style='background-color: #faa33e; border: 1px solid #faa33e;' class="chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase"><span class="chicklet__content"><span><img src="{{ asset("/images/medium.svg") }}"><span class="with-icon"><?= $task->priority; ?></span></span>
                                                        </span>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($task->priority == "Low") { ?>
                                                    <div title="Low"  style='background-color: #198c62; border: 1px solid #198c62;' class="chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase"><span class="chicklet__content"><span><img src="{{ asset("/images/low.svg") }}"><span class="with-icon"><?= $task->priority; ?></span></span>
                                                        </span>
                                                    </div>
                                                    <?php } ?>
                                                    <!--<div title="Argos Help Desk" class="avatar avatar--algae-green" style="width: 20px; height: 20px; border-radius: 50%;"><span style="font-size: 13.3333px; font-weight: 500; color: rgb(255, 255, 255);">A</span></div>-->
                                                </div>
                                            </li>
                                            <?php } ?> 
                                           
                                        </ul>
                                    </div>
                                    <!-- data-sidebar-->
                                </div>
                                 
                                <!-- end card-body-->
                            </div>
                            <!-- end card-->
                        </div>
                        <!-- end col -->
                       
    
  
    <?php } } ?>
                     

                    </div>
                </div>
                <!-- kanvan view end -->
            </div>
            <!-- End Page-content -->



@endsection


