@extends('layouts.app')

@section('content')

 <div class="page-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="page-title-box d-flex align-items-center justify-content-between">
					<h4 class="mb-0">Workflow List</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
							<li class="breadcrumb-item active">Workflow List</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div  id='workflowList'>
			
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8 offset-md-2">
								<div class="mt-2"> <a href="{{route('manage-workflow')}}" class="btn btn-success waves-effect waves-light mb-0">Add Workflow</a>
										</div>
										<hr/>
										<div class="table-responsive">
											<table class="table table-bordered mb-0">
												<thead>
													<tr>
														<th>Workflow</th>
														<th>Created On</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													
													@foreach($list as $data)
													<tr>
														<td>{{ $data->workflow_name }}</td>
														<td> <?php  $date = date_create($data->created_at); ?>
                                                               {{ date_format($date,"Y/m/d") }}
                                                        </td>
														<td>
														    <a href="{{route('manage-workflow',[base64_encode($data->id)])}}" class="btn btn-link waves-effect p-0"><i class='uil-pen'></i></a>
														    <a class="btn delete btn-link waves-effect p-0 sectiondeleteworkflow" delete-id="{{ $data->id }}" delete-table="workflows"><i class='uil-trash-alt'></i></a>
														
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								
							
						</div>
					
				</div>
				</div>
			</div>
		
	</div>
</div>




@endsection