  @extends('layouts.app')

  @section('content')

    <div class="page-content workflow-add-container">
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-lg-8 offset-lg-2">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Add Work Flow</h4>
                    
                    
                    
                    
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
							<li class="breadcrumb-item"><a href="{{route('workflow')}}" class="">Workflow List</a></li>
                            <li class="breadcrumb-item active">Add workflow</li>
                        </ol>
                    </div>
                </div>
            </div>
			
        </div>
		<div class='row'>
		<div class='col-lg-8 offset-lg-2'>
			<?php if(isset($work[0]->id)){ ?>
                        <div class="alert alert-success alert-dismissible fade hide" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Workflow updated successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                     <?php } else { ?>   
                         <div class="alert alert-success alert-dismissible fade hide" role="alert">
                            <i class="uil uil-check me-2"></i>
                           Workflow inserted successfully
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            
                            </button>
                        </div>
                        
                        <?php } ?>
			</div>
		</div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="card">
                    <div class="card-body">
					<div class='row'>
					<div class='col-md-12'>
					<div class="mt-2"> <!--a href="{{route('manage-workflow')}}" class="btn btn-outline-primary waves-effect waves-light mb-3">Add Workflow</a-->
									<a href="{{route('workflow')}}" class="btn btn-outline-primary waves-effect waves-light mb-0">Workflow List</a>
                                                        <br> <!--<a href="#" class="btn btn-outline-primary waves-effect waves-light mb-3">Project Workflow List</a>--> </div>
														<hr/>
					</div>
					</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mt-0">
                                    <form name="workflowform" method="post">
                                        @csrf
                                        <div class="mb-3">
                                            <label class="form-label" for="formrow-Fullname-input">Workflow Name</label>
                                            <input type="text" name="workflowname" class="form-control" id="formrow-Fullname-input" placeholder="Enter Workflow Name" value="<?php echo isset($work[0]->workflow_name)? $work[0]->workflow_name:''; ?>">
                               <?php if(isset($work[0]->id)){ ?>
                                    <input type="hidden" name="workid" value="<?php echo $work[0]->id; ?>">
                                 <?php } ?>

                                             </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                         <label class="form-label" for="formrow-Fullname-input">Workflow Status</label> </div>
                                                    <div class="col-md-12 renderhtmlwork">

                                                       
                                                            <?php if(!empty($workstatus)) { ?>
                                                              <?php $te = 1; foreach($workstatus as $worksta){ ?>
                                                          <div class="mb-3 <?php if($te > 1){ ?> workflowstart <?php } ?> colorSelector">
                                                                <input type="text" name="workflowstatus[]" class="form-control" placeholder="Name (e.g. Open, In Progress, etc.)" value="<?php echo $worksta->statusname; ?>">
                                                                <input type="color" name="color[]" value="<?php echo $worksta->color; ?>"> 
                                                                <input type="hidden" name="workflowstatusid[]" value="<?php echo $worksta->id; ?>"> 
                                                          </div>
                                                                <?php if($te > 1) { ?>
                                                             <div class='workflowremove' workflow-id="<?php echo $worksta->id; ?>"><button type='button' class='btn btn-primary'>-</button></div>
                                                                <?php } ?>

                                                              <?php $te++; } ?>

                                                            <?php } else { ?> 
                                                            <div class="mb-3 ">
                                                            <input type="text" name="workflowstatus[]" class="form-control" placeholder="Name (e.g. Open, In Progress, etc.)">                                                            <input type="checkbox" name="cust[1]" value="1">
                                                            <input type="color" name="color[]">
                                                             </div>
                                                            <?php } ?>

                                                       

                                                    </div>
                                                    
                                                    
                                                    <div class="col-md-12">
                                                        <button type="button" data-attr="1" class="btn btn-outline-secondary waves-effect addhtmlwork"> + Add Another </button>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div class="d-flex flex-wrap gap-3 mt-5">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light w-md" id="formbut"> <?php if(isset($work[0]->id)){ ?>Update Workflow <?php } else { ?> Create Workflow <?php } ?></button>
                                            <?php if(isset($work[0]->id)){ ?> <?php } else { ?><button type="reset" class="btn btn-outline-danger waves-effect waves-light w-md resetdataall">Reset</button> <?php } ?>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

            @endsection
         

