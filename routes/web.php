<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cache-clear', function() {
Artisan::call('cache:clear');
return "Cache is cleared";
});
Route::get('/config-cache', function() {
Artisan::call('config:cache');
return "config Cache is cleared";
});
Route::get('/config-clear', function() {    
Artisan::call('config:clear');
return "confid is cleared";
});

Route::get('/cretecont', function() {    
Artisan::call('make:controller Tasklink');
return "controller create";
});

Route::get('/cretemodel', function() {    
Artisan::call('make:model Project');
return "model create";
});




Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('send-mail123', function () {
   
    $details = [
        'title' => 'Testing email',
        'body' => 'This is for testing email using smtp'
    ];
   
    \Mail::to('7417rohitarora@gmail.com')->send(new \App\Mail\MyTestMail($details));
   
    dd("Email is Sent.");
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'Profile'])->name('profile');
Route::post('/update-profile', [App\Http\Controllers\HomeController::class, 'Updateprofile'])->name('update-profile');
Route::post('/password-change', [App\Http\Controllers\HomeController::class, 'Passwordchange'])->name('password-change');
Route::get('/tasklink/{id}/{token}', [App\Http\Controllers\Tasklink::class, 'Tasklink'])->name('tasklink');
Route::get('/report', [App\Http\Controllers\HomeController::class, 'Report'])->name('report');
Route::post('/ckimage', [App\Http\Controllers\HomeController::class, 'Ckimage'])->name('ckimage');

//Route::get('ckeditor', 'CkeditorController@index');
//Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');





Route::get('/workflow', [App\Http\Controllers\WorkflowController::class, 'Getworkflow'])->name('workflow');
Route::get('/manage-workflow/{id?}', [App\Http\Controllers\WorkflowController::class, 'Manageworkflow'])->name('manage-workflow');
Route::post('/workflow-save', [App\Http\Controllers\WorkflowController::class, 'Saveworkflow'])->name('workflow-save');
Route::post('/commondelete', [App\Http\Controllers\WorkflowController::class, 'Commondelete'])->name('commondelete');
Route::post('/workflowdelete', [App\Http\Controllers\WorkflowController::class, 'Workflowdelete'])->name('workflowdelete');
Route::post('/sectiondisable', [App\Http\Controllers\WorkflowController::class, 'Sectiondisable'])->name('sectiondisable');
Route::get('/tasklog', [App\Http\Controllers\WorkflowController::class, 'Tasklog'])->name('tasklog');

Route::get('/project', [App\Http\Controllers\ProjectController::class, 'Getproject'])->name('project');
Route::get('/manage-project/{id?}', [App\Http\Controllers\ProjectController::class, 'Manageproject'])->name('manage-project');
Route::post('/project-save', [App\Http\Controllers\ProjectController::class, 'Saveproject'])->name('project-save');
Route::post('/project-delete', [App\Http\Controllers\ProjectController::class, 'Deleteproject'])->name('project-delete');
Route::post('/company-save', [App\Http\Controllers\ProjectController::class, 'Companysave'])->name('company-save');
Route::post('/task-save', [App\Http\Controllers\ProjectController::class, 'Tasksave'])->name('task-save');
Route::post('/task-image', [App\Http\Controllers\ProjectController::class, 'Taskimage'])->name('task-image');
Route::get('/list-task/{project?}/{status?}', [App\Http\Controllers\ProjectController::class, 'Tasklist'])->name('list-task');
Route::post('/deletetask-image', [App\Http\Controllers\ProjectController::class, 'Deletetaskimage'])->name('deletetask-image');
Route::post('/task-image', [App\Http\Controllers\ProjectController::class, 'Taskimage'])->name('task-image');
Route::post('/taskcomment-save', [App\Http\Controllers\ProjectController::class, 'Taskcommentsave'])->name('taskcomment-save');
Route::post('/getfilter-data', [App\Http\Controllers\ProjectController::class, 'Getfilterdata'])->name('getfilter-data');
Route::post('/getreport-data', [App\Http\Controllers\ProjectController::class, 'Getreportdata'])->name('getreport-data');
Route::get('/manage-task/{projectid}/{id?}', [App\Http\Controllers\ProjectController::class, 'Addtask'])->name('manage-task');



