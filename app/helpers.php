<?php 

if (! function_exists('insertintotablewithlastid')) {
	function insertintotablewithlastid($table,$data) {
		$lastid = DB::table($table)->insertGetId($data); 
		if($lastid)
		{
			return $lastid;
		}else{
			return false;
		}

	}
}

if (! function_exists('insertintotable')) { 
	function insertintotable($table,$data) {
		$inserteddata = DB::table($table)->insert($data);
		return $inserteddata; 
    }
}

if (! function_exists('gettablerow')) {
	function gettablerow($table,$where = Null) {
	    if(!empty($where)){
	       $getdata = DB::table($table)->select('*')->where($where)->get();  
	    }else{
	        $getdata = DB::table($table)->select('*')->get();  
	    }
	 	return $getdata; 
    }
}

if (! function_exists('gettablerowspecificuser')) {
	function gettablerowspecificuser($table,$where = Null,$select,$user) {
	    
	    $where1 = array("projectstatus"=>1);
	   	$getdata = DB::table('projects')->select('*')->where($where1)->get();
	   	
	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
	   	$userid = $getuser[0]->id;
	   	
	   	$projectid = array();
	   	foreach($getdata as $getd)
	   	{
	   	   $inviteuser = explode(",",$getd->inviteclientusers);
	   	   foreach($inviteuser as $invite){
	   	      if($invite == $userid)
	   	      {
	   	          $projectid[] = $getd->id;
	   	      }
	   	   }
	   	   
	   	   if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
	   	   {
	   	       $projectid[] = $getd->id;
	   	   }
	   	}
	    
	    if(!empty($where)){
	       $getdata = DB::table($table)->select($select)->where($where)->whereIn('projectid', $projectid)->get(); 
	    }else{
	        $getdata = DB::table($table)->select($select)->get();  
	    }
	 	return $getdata; 
    }
}

if (! function_exists('gettablerowspecific')) {
	function gettablerowspecific($table,$where = Null,$select) {
	    if(!empty($where)){
	       $getdata = DB::table($table)->select($select)->where($where)->get(); 
	    }else{
	        $getdata = DB::table($table)->select($select)->get();  
	    }
	 	return $getdata; 
    }
}

if (! function_exists('gettablerowgroupby')) {
	function gettablerowgroupby($table,$where,$groupby) {
	   	$getdata = DB::table($table)->select($groupby, DB::raw('count(*) as total'))->where($where)->groupBy($groupby)->get();
		return $getdata; 
    }
}

if(!function_exists('create_order_number')){
    function create_order_number(){
        $number = str_pad(1,3,'0',STR_PAD_LEFT);
        $order_number = '#'.$number;
        $order_status = true;
        while ($order_status) {
            $check_order_number = DB::table("projecttasks")->select('*')->where(array("taskordernumber"=>$order_number))->get();
            if(sizeof($check_order_number) > 0){
            $number = str_pad(++$number,3,'0',STR_PAD_LEFT);
            $order_number = '#'.$number; 
            $order_status = true;
            }else{
            $order_status = false;
            }
        }
        return $order_number;
    }
}

if (! function_exists('gettablerowgroupbyuser')) {
	function gettablerowgroupbyuser($table,$where,$groupby,$user) {
	    $where1 = array("projectstatus"=>1);
	   	$getdata = DB::table('projects')->select('*')->where($where1)->get();
	   	
	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
	   	$userid = $getuser[0]->id;
	   	
	   	$projectid = array();
	   	foreach($getdata as $getd)
	   	{
	   	   $inviteuser = explode(",",$getd->inviteclientusers);
	   	   foreach($inviteuser as $invite){
	   	      if($invite == $userid)
	   	      {
	   	          $projectid[] = $getd->id;
	   	      }
	   	   }
	   	   
	   	   if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
	   	   {
	   	       $projectid[] = $getd->id;
	   	   }
	   	}
	   	$getdata = DB::table($table)->select($groupby, DB::raw('count(*) as total'))->where($where)->whereIn('projectid', $projectid)->groupBy($groupby)->get();
		return $getdata; 
    }
}

if (! function_exists('getAllTaskList')) {
    function getAllTaskList($project = Null,$status = Null){
        if(!empty($project ) && empty($status)){
            
          $project = base64_decode($project);
             $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
            ->where('t1.projectid', '=',$project)
            ->where('p.projectstatus', '=',1)
            ->groupBy('t1.id')
            ->orderBy('t1.taskorderby', 'DESC')
            ->get();
            return $list;
        } else if(!empty($project ) && !empty($status)){
            
             $project = base64_decode($project);
             $status = base64_decode($status);
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
            ->where('t1.projectid', '=',$project)
            ->where('t1.workflowstatus', '=',$status)
            ->where('p.projectstatus', '=',1)
            ->groupBy('t1.id')
            ->orderBy('t1.taskorderby', 'DESC')
            ->get();
        return $list; 
        
        } else {
            
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
             ->where('p.projectstatus', '=',1)
              ->groupBy('t1.id')
            ->orderBy('t1.taskorderby', 'DESC')
            ->get();
            return $list;
        }
      
 }
}

if (! function_exists('getAllTaskListbyuser')) {
    function getAllTaskListbyuser($project = Null,$status = Null,$user){
        
         $where1 = array("projectstatus"=>1);
    	   	$getdata = DB::table('projects')->select('*')->where($where1)->get();
    	   	
    	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
    	   	$userid = $getuser[0]->id;
    	   	
    	   	$projectid = array();
    	   	foreach($getdata as $getd)
    	   	{
    	   	   $inviteuser = explode(",",$getd->inviteclientusers);
    	   	   foreach($inviteuser as $invite){
    	   	      if($invite == $userid)
    	   	      {
    	   	          $projectid[] = $getd->id;
    	   	      }
    	   	   }
    	   	   
    	   	   if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
    	   	   {
    	   	       $projectid[] = $getd->id;
    	   	   }
    	   	}
      
        if(!empty($project ) && empty($status)){
            
          $project = base64_decode($project);
             $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
             ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
            ->where('t1.projectid', '=',$project)
            ->where('p.projectstatus', '=',1)
            ->groupBy('t1.id')
             ->orderBy('t1.taskorderby', 'DESC')
            ->get();
            return $list;
            
        } else if(!empty($project ) && !empty($status)){
            
             $project = base64_decode($project);
             $status = base64_decode($status);
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
              ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
            ->where('t1.projectid', '=',$project)
            ->where('t1.workflowstatus', '=',$status)
            ->where('p.projectstatus', '=',1)
            ->groupBy('t1.id')
             ->orderBy('t1.taskorderby', 'DESC')
            ->get();
        return $list; 
        
        } else {
            
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid')
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'))
            ->whereIn('t1.projectid', $projectid)
            ->where('p.projectstatus', '=',1)
            ->groupBy('t1.id')
             ->orderBy('t1.taskorderby', 'DESC')
            ->get();
            return $list;
        }
      
 }
}

if (! function_exists('getAllTaskListbystatus')) {
    function getAllTaskListbystatus($id = Null,$project= Null){
         $project = base64_decode($project);
       if(!empty($project ) && empty($id)){ 
      $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->where('t1.projectid', '=',$project)
            ->where('p.projectstatus', '=',1)
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name')
            
            ->get();
        return $list;
       } else if(!empty($project ) && !empty($id)){
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->where('t1.workflowstatus', '=',$id)
            ->where('t1.projectid', '=',$project)
            ->where('p.projectstatus', '=',1)
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name')
            ->get();
        return $list;
       }else {
            $list = DB::table('projecttasks as t1')
            ->leftjoin('projects as p', 't1.projectid','=','p.id')
            ->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id')
            ->where('t1.workflowstatus', '=',$id)
            ->where('p.projectstatus', '=',1)
            ->select('t1.*','p.projectname as project_name','c.statusname as status_name')
            ->get();
            return $list;
       }
 }
}

if(! function_exists('getsearchstatus'))
{
   function getsearchstatus($user = Null)
   {
      if($user != ""){
        $where1 = array("projectstatus"=>1);
	   	$getdata = DB::table('projects')->select('*')->where($where1)->get();
	   	
	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
	   	$userid = $getuser[0]->id;
	   	
	   	$projectid = array();
	   	foreach($getdata as $getd)
	   	{
	   	   $inviteuser = explode(",",$getd->inviteclientusers);
	   	   foreach($inviteuser as $invite){
	   	      if($invite == $userid)
	   	      {
	   	          $projectid[] = $getd->id;
	   	      }
	   	   }
	   	   
	   	   if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
	   	   {
	   	       $projectid[] = $getd->id;
	   	   }
	   	}
	   	
	   
	   	
        $project = DB::table('projects')->select('*')->whereIn("id",$projectid)->get(); 
        
      	$rr = array();
      	foreach($project as $pro)
      	{
      	   $rr[] =  $pro->workflowname;
      	}
      	
        $list =	DB::table('workflowstatuss')->select('*')->whereIn('workflowsid', $rr)->get();
        return $list;  
      }else{
      	$project = DB::table('projects')->select('*')->where(array("projectstatus"=>1))->get(); 
      	$rr = array();
      	foreach($project as $pro)
      	{
      	   $rr[] =  $pro->workflowname;
      	}
      	
        $list =	DB::table('workflowstatuss')->select('*')->whereIn('workflowsid', $rr)->get();;
        return $list;
      }
   }
}
if (! function_exists('getreporttaskhelper')) {
    function getreporttaskhelper($selectproject= Null,$selectstatus= Null,$selectfrom= Null,$selectto= Null){
		
        $query = DB::table("projecttasks as t1");
		$query->leftjoin('projects as p', 't1.projectid','=','p.id');
		$query->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id');
		$query->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid');
		$query->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts')); 
		$query->groupBy('t1.id');
		$query->where('p.projectstatus', '=',1); 
		if (!empty($selectstatus)) { 
		$query->where('t1.workflowstatus', $selectstatus);
		} 
		if (!empty($selectproject)) {
		$query->where('t1.projectid', $selectproject);
		}
        if(!empty($selectfrom) && !empty($selectto))
		{
			 $date=date_create($selectfrom);
             $from = date_format($date,"Y-m-d H:i:s");
			 $date1=date_create($selectto);
             $to = date_format($date1,"Y-m-d H:i:s");
			   
		$query->whereBetween('t1.created_at', [$from, $to]);
		}			
		$result= $query->get();	
		
		return $result; 
	}
}
			
if (! function_exists('getreporttaskhelperuser')) {
function getreporttaskhelperuser($selectproject= Null,$selectstatus= Null,$selectfrom= Null,$selectto= Null,$user= Null){
$where1 = array("projectstatus"=>1);
$getdata = DB::table('projects')->select('*')->where($where1)->get();
$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
$userid = $getuser[0]->id;
$projectid = array();
foreach($getdata as $getd)	{
$inviteuser = explode(",",$getd->inviteclientusers);
foreach($inviteuser as $invite){
	if($invite == $userid)
		{
			$projectid[] = $getd->id;
			}
			}
			if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)	
				{	
			$projectid[] = $getd->id;
			}	
			}   
			$query = DB::table("projecttasks as t1"); 
			$query->leftjoin('projects as p', 't1.projectid','=','p.id');
			$query->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id');  
			$query->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid'); 
			$query->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'));  
			$query->groupBy('t1.id'); 
			if (!empty($selectstatus)) { 
			$query->where('t1.workflowstatus', $selectstatus);
			} 
			if (!empty($selectproject)) {
			$query->where('t1.projectid', $selectproject);
			}
			if(!empty($selectfrom) && !empty($selectto))
			{
				 $date=date_create($selectfrom);
				 $from = date_format($date,"Y-m-d H:i:s");
				 $date1=date_create($selectto);
				 $to = date_format($date1,"Y-m-d H:i:s");
				   
			$query->whereBetween('t1.created_at', [$from, $to]);
			}
			$result= $query->get(); 
			return $result;
	   }
  }
  
if (! function_exists('getfiltertaskhelper')) {
    function getfiltertaskhelper($projecttakevalue= Null,$filtertitle= Null,$filterassign= Null,$sortby= Null,$selectfilterstatus= Null,$selectfilterpriority = Null){
        $query = DB::table("projecttasks as t1");
            $query->leftjoin('projects as p', 't1.projectid','=','p.id');
            $query->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id');
            $query->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid');
            $query->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'));
            $query->groupBy('t1.id');
            $query->where('p.projectstatus', '=',1);
            if (!empty($selectfilterstatus)) {
                $query->where('t1.workflowstatus', $selectfilterstatus);
            }
            if (!empty($selectfilterpriority)) {
                $query->where('t1.priority', $selectfilterpriority);
            }
            if (!empty($sortby)) {
                $query->orderBy('id', $sortby);
            }
            if (!empty($projecttakevalue)) {
                $query->where('t1.projectid', $projecttakevalue);
            } 
            if (!empty($filtertitle)) {
                $query->where('t1.tasktitle', 'like', '%'.$filtertitle.'%');
                $query->orWhere('t1.taskordernumber', 'like', '%'.$filtertitle.'%');
            } 
            if (!empty($filterassign)) {
                $query->where('t1.assignto', $filterassign);
            }
            $result= $query->get();
            return $result;     
    }
}

if (! function_exists('getfiltertaskhelperuser')) {
    function getfiltertaskhelperuser($projecttakevalue= Null,$filtertitle= Null,$filterassign= Null,$sortby= Null,$user= Null,$selectfilterstatus= Null,$selectfilterpriority = Null){
        
     $where1 = array("projectstatus"=>1);
	   	$getdata = DB::table('projects')->select('*')->where($where1)->get();
	   	
	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get();
	   	$userid = $getuser[0]->id;
	   	
	   	$projectid = array();
	   	foreach($getdata as $getd)
	   	{
	   	   $inviteuser = explode(",",$getd->inviteclientusers);
	   	   foreach($inviteuser as $invite){
	   	      if($invite == $userid)
	   	      {
	   	          $projectid[] = $getd->id;
	   	      }
	   	   }
	   	   
	   	   if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
	   	   {
	   	       $projectid[] = $getd->id;
	   	   }
	   	}
        
        
        $query = DB::table("projecttasks as t1");
            $query->leftjoin('projects as p', 't1.projectid','=','p.id');
            $query->leftjoin('workflowstatuss as c', 't1.workflowstatus','=','c.id');
            $query->leftjoin('project_taskcomments as d', 't1.id','=','d.taskid');
            $query->select('t1.*','p.projectname as project_name','c.statusname as status_name','c.color as color',DB::raw('count(d.id) as commentscounts'));
            $query->groupBy('t1.id');
            if (!empty($sortby)) {
                $query->orderBy('id', $sortby);
            }
            if (!empty($selectfilterstatus)) {
                $query->where('t1.workflowstatus', $selectfilterstatus);
            }
            if (!empty($selectfilterpriority)) {
                $query->where('t1.priority', $selectfilterpriority);
            }
            if (!empty($projecttakevalue)) {
                $query->where('t1.projectid', $projecttakevalue);
            }else{
                $query->whereIn('t1.projectid', $projectid);
            } 
            if (!empty($filtertitle)) {
                $query->where('t1.tasktitle', 'like', '%'.$filtertitle.'%');
                 $query->orWhere('t1.taskordernumber', 'like', '%'.$filtertitle.'%');
            } 
            if (!empty($filterassign)) {
                $query->where('t1.assignto', $filterassign);
            }
            $result= $query->get();
            return $result;     
    }
}


if (! function_exists('getuserdetail')) {
    function getuserdetail($id){
      	$getdata = DB::table("users")->select('*')->where("id","=",$id)->get();
		return $getdata; 
 }
}



if (! function_exists('deletetablerow')) {
	function deletetablerow($table,$where) {
		$deletedata = DB::table($table)->where($where)->delete();
		return true; 
    }
}

if (! function_exists('updatetablerow')) {
	function updatetablerow($table,$where,$data) {
		$updatedata = DB::table($table)->where($where)->update($data);
		return true; 
    }
}

if (! function_exists('getprojectbyuserid')) {
	function getprojectbyuserid($table,$user) {
	    $where = array("projectstatus"=>1);
	   	$getdata = DB::table($table)->select('*')->where($where)->get();
	   	
	   	$getuser = DB::table("users")->select('*')->where("id","=",$user)->get(); 
	   	$userid = $getuser[0]->id;
	   	
	   	$projectid = array(); 
	   	foreach($getdata as $getd)
	   	{
	   	   $inviteuser = explode(",",$getd->inviteclientusers);
	   	   foreach($inviteuser as $invite){
	   	      if($invite == $userid)
	   	      {
	   	          $projectid[] = $getd->id;
	   	      }
	   	   }
	   	  if($userid == $getd->notify1 || $userid == $getd->notify2 || $userid == $getd->notify3 || $userid == $getd->notify4 || $userid == $getd->notify5 || $userid == $getd->notify6 || $userid == $getd->notify7 || $userid == $getd->notify8 || $userid == $getd->notify9 || $userid == $getd->notify10)
	   	   {
	   	       $projectid[] = $getd->id;
	   	   }
	   	   
   	   	   if($getd->created_by == $userid)
    	   {
    	   	 $projectid[] = $getd->id;   
    	   }
	   	}
	   	$userproject = DB::table('projects')->whereIn('id', $projectid)->get();
	   	return $userproject; 
    }
}

?>