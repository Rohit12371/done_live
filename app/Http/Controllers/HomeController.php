<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $user = Auth::user()->id;
         if($user == 1){
             $project = gettablerow("projects",array("projectstatus"=>1)); 
         }else{
             $project = getprojectbyuserid("projects",$user);
         }
         return view('home',compact('project'));
    }
    
    function Profile(){
       $user = Auth::user()->id; 
       $userlogin = gettablerow("users",array("id"=>$user));
       return view('profile.profile',compact('userlogin'));
    }
    
    function Updateprofile(Request $request)
    {
        $profilename =  $request->get('profilename');
        $profileupdateid =  $request->get('profileupdateid');
        $profilephone =  $request->get('profilephone');
        $updateprofile = updatetablerow("users",array("id"=>$profileupdateid),array("name"=>$profilename,"phone"=> $profilephone));
        if($updateprofile)
        {
            $rec = array("status"=>true,"msg"=>"Profile successfully updated");
            echo json_encode($rec);
        }
    }
    
    function Report($project = Null,$status = Null)
    {
       $user = Auth::user()->id;
        if($user == 1){
            $alltask = getAllTaskList($project,$status);
            $allassign = array();
            $getsearchstatus = getsearchstatus();
            $allproject = gettablerow("projects",array("projectstatus"=>1));
            $allstatus = gettablerow("workflowstatuss",array("status"=>1));
            $taskproject = $project;
            if(!empty($project)){
                $filter = "notshow";
            }else{
                $filter = "show";
            }
        }else{
            $alltask = getAllTaskListbyuser($project,$status,$user);
          
            $allassign = array();
            $getsearchstatus = getsearchstatus($user);
            $allproject = getprojectbyuserid("projects",$user); 
            $allstatus = gettablerow("workflowstatuss",array("status"=>1));
            $taskproject = $project;
            if(!empty($project)){
                $filter = "notshow";
            }else{
                $filter = "show";
            }
        }
       return view('project.reportlist', compact(['alltask','allproject','allassign','getsearchstatus','allstatus','filter','taskproject','user'])); 
    }
    
    function Getprojectstatus(Request $request)
	{
		$projectid =  $request->get('projectid');
		$getproject =  gettablerow("projects",array("id"=>$projectid));
		$workflow = $getproject[0]->workflowname;
		$getworkflowstatus =  gettablerow("workflowstatuss",array("workflowsid"=>$workflow));
		$html = "<label class='form-label' >Select Status</label>";
		$html .="<select name='selectstatus' class='form-select selecthtmlclass'>";
		$html .= "<option value=''>Select status</option>";
		foreach($getworkflowstatus as $getsta)
		{
		  $html .= "<option value=".$getsta->id.">".$getsta->statusname."</option>";	
		}
		$rec = array('status'=>true,'html'=>$html);
		echo json_encode($rec);
	}
    
     function Passwordchange(Request $request)
     {
        $oldpassword =  $request->get('oldpassword');
        $newpassword =  $request->get('newpassword');
        $passwordupdateid =  $request->get('passwordupdateid');
        
        $getoldpass =  gettablerow("users",array("id"=>$passwordupdateid));
        $datbasepass =  $getoldpass[0]->password;
        $old = Hash::make($oldpassword);
       
	   if(Hash::check($oldpassword,$datbasepass)) {
            $new = Hash::make($newpassword);
           
            $updatepassword = updatetablerow("users",array("id"=>$passwordupdateid),array("password"=> $new));
            if($updatepassword)
            {
                $rec = array("status"=>true,"msg"=>"Password successfully updated");
                echo json_encode($rec);
            }
        } else {
            
              $rec = array("status"=>false,"msg"=>"Old password not match");
              echo json_encode($rec);
        }
       
      }
	  
	  function Ckimage(Request $request)
	  {
		 if($request->hasFile('upload')) {
     
        //get filename with extension
        $fileNameWithExtension = $request->file('upload')->getClientOriginalName();
   
        //get filename without extension
        $fileName = pathinfo($fileNameWithExtension, PATHINFO_FILENAME);
   
        //get file extension
        $extension = $request->file('upload')->getClientOriginalExtension();
   
        //filename to store
        $fileNameToStore = $fileName.'_'.time().'.'.$extension;
   
        //Upload File
		$request->file('upload')->move(public_path('images'), $fileNameToStore);
        //$request->file('upload')->storeAs(public_path('images'), $fileNameToStore);
 
        $CKEditorFuncNum = $request->input('CKEditorFuncNum') ? $request->input('CKEditorFuncNum') : 0;
         
        if($CKEditorFuncNum > 0){
         
            $url = asset('images/'.$fileNameToStore); 
            $msg = 'Image successfully uploaded'; 
            $renderHtml = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
             
            // Render HTML output 
            @header('Content-type: text/html; charset=utf-8'); 
            echo $renderHtml;
             
        } else {
         
            $url = asset('images/'.$fileNameToStore); 
            $msg = 'Image successfully uploaded'; 
            $renderHtml = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            return response()->json([
                'uploaded' => '1',
                'fileName' => $fileNameToStore,
                'url' => $url
            ]);
        }
         
    }
  }
}
