<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

use Mail;
use Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; 
use Auth;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function Getproject()
    {
        $user = Auth::user()->id;
        if($user == 1){
            if($user == 1){
              $list =  gettablerow("projects");
            }else{
              $list =  getprojectbyuserid("projects",$user); 
            }
            return view('project.list', compact('list')); 
        }else{
            $headerproject =  gettablerow("projects",array("projectstatus"=>1));
              $checkadmin = array();
              foreach($headerproject as $header){
                  if($header->usertype1 =="Administrator"){
                      if($header->notify1 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                  if($header->usertype2 =="Administrator"){
                      if($header->notify2 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                  if($header->usertype3 =="Administrator"){
                      if($header->notify3 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype4 =="Administrator"){
                      if($header->notify4 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype5 =="Administrator"){
                      if($header->notify5 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype6 =="Administrator"){
                      if($header->notify6 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype7 =="Administrator"){
                      if($header->notify7 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype8 =="Administrator"){
                      if($header->notify8 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype9 =="Administrator"){
                      if($header->notify9 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                  if($header->usertype10 =="Administrator"){
                      if($header->notify10 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
              }
              if(!empty($checkadmin)){
                if($user == 1){
                  $list =  gettablerow("projects",array("projectstatus"=>"1"));
                }else{
                  $list =  getprojectbyuserid("projects",$user); 
                }
                return view('project.list', compact('list'));  
              }else{
                  return redirect()->route('home');
              }
        }
        
    }
    
    function Addtask($projectid,$id = Null)
    {
       $user = Auth::user()->id;
       $usertype =  gettablerow("users",array("id"=> $user));
       $utype = $usertype[0]->usertype;
       $projectid = base64_decode($projectid);
       $project = gettablerow("projects",array("id"=>$projectid));
       if($project[0]->projectstatus == 1){
       $workflowstatus = gettablerow("workflowstatuss",array("workflowsid"=>$project[0]->workflowname));
       $allusers =  gettablerow("users",array("userstatus"=>1));
        if(!empty($id))
        {
            $id = base64_decode($id);
            $taskrep =  gettablerow("projecttasks",array('id'=>$id));
            $taskcomment = gettablerow("project_taskcomments",array('taskid'=>$id));
            return view('project.task',compact('project','workflowstatus','taskrep','taskcomment','allusers','utype','user'));
        }else{
            return view('project.task',compact('project','workflowstatus','allusers','utype','user'));
        }
       }else{
          return redirect()->route('home'); 
       }
    }
    
    function Deletetaskimage(Request $request){
       $id =  $request->get('deleteid');
       $deleteimage =  deletetablerow("projecttask_images",array("id"=>$id));
       if($deleteimage){
           $record = array("status"=>true,"msg"=>"deleted successfully");
           echo json_encode($record);
       }
    }
    
    function Manageproject($id = Null)
    {
         $user = Auth::user()->id;
        if($user == 1){
            $company = gettablerow("Companies",array("companystatus"=>1));
            $workflow = gettablerow("workflows",array("workflowstatus"=>1));
            $allusers =  gettablerow("users",array("userstatus"=>1,"usertype"=>3));
            if(!empty($id))
            {
                $id = base64_decode($id);
                $project =  gettablerow("projects",array('id'=>$id));
                return view('project.add', compact(['project','company','workflow','allusers']));
            }else{
                 return view('project.add', compact(['company','workflow','allusers']));
            }
        }else{
             $headerproject =  gettablerow("projects",array("projectstatus"=>1));
              $checkadmin = array();
              foreach($headerproject as $header){ 
                  if($header->usertype1 =="Administrator"){
                      if($header->notify1 == Auth::user()->id) 
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                  if($header->usertype2 =="Administrator"){
                      if($header->notify2 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                 if($header->usertype3 =="Administrator"){
                      if($header->notify3 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype4 =="Administrator"){
                      if($header->notify4 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype5 =="Administrator"){
                      if($header->notify5 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype6 =="Administrator"){
                      if($header->notify6 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype7 =="Administrator"){
                      if($header->notify7 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype8 =="Administrator"){
                      if($header->notify8 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                   if($header->usertype9 =="Administrator"){
                      if($header->notify9 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
                  
                  if($header->usertype10 =="Administrator"){
                      if($header->notify10 == Auth::user()->id)
                      {
                         $checkadmin[]="present"; 
                      }
                  }
              }
              if(!empty($checkadmin)){
                $company = gettablerow("Companies",array("companystatus"=>1));
                $workflow = gettablerow("workflows",array("workflowstatus"=>1));
                $allusers =  gettablerow("users",array("userstatus"=>1));
                if(!empty($id))
                {
                    $id = base64_decode($id);
                    $project =  gettablerow("projects",array('id'=>$id));
                    return view('project.add', compact(['project','company','workflow','allusers']));
                }else{
                     return view('project.add', compact(['company','workflow','allusers']));
                }  
              }else{
                  return redirect()->route('home');
              }
            
        }
    }
    
    function Companysave(Request $request)
    {
       $companyname =  $request->get('companynamepop');
       $data = array(
                        'companyname'=> $companyname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
        $insertrecord = insertintotable('Companies',$data);
        $company = gettablerow("Companies",array("companystatus"=>1));
        $html ="";
        $html .= "<select name='companyname' class='form-control'> <option value=''>Select Company</option>";
         foreach($company as $comp){
             $html .= "<option value='".$comp->id."'>".$comp->companyname."</option>";
         }
         $html .="</select>";
             if($insertrecord){
                  $response = array(
                     "status"=>true,
                     "message"=>"Company successfully insert",
                     "record"=> $html
                 );
                 echo json_encode($response);
                 die();}
    } 
    
       function Tasksave(Request $request)
        {
           $projectname =  $request->get('projectname');
           $projectid =  $request->get('projectid');
           $tasktitle =  $request->get('tasktitle');
           $priority =  $request->get('priority');
           $workflowstatus =  $request->get('workflowstatus');
           $dueto =  $request->get('dueto');
           $assignto =  $request->get('assignto');
           $tags =  $request->get('tags');
           $description =  $request->get('editordata');
           $images =  $request->get('taskimage');
           $updateid = $request->get('taskid');
           $created_by = Auth::user()->id;
           
           $userdata = gettablerow("users",array("id" => $created_by));
           
           if(!empty($updateid)){
               
               $olddata = gettablerow("projecttasks",array("id" => $updateid));
               
               if($olddata[0]->tasktitle != $tasktitle)
               {
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update task title ".$olddata[0]->tasktitle." to ".$tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update task title for ".$tasktitle." in ".$projectname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update task title for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               if($olddata[0]->priority != $priority)
               {
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update priority ".$olddata[0]->priority." to ".$priority." for task title ".$olddata[0]->tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update priority for ".$tasktitle." in ".$projectname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update priority for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               if($olddata[0]->workflowstatus != $workflowstatus)
               {
                    $oldflow =  gettablerow("workflowstatuss",array("id"=>$olddata[0]->workflowstatus));
                    $newflow =  gettablerow("workflowstatuss",array("id"=>$workflowstatus));
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update work flow status ".$oldflow[0]->statusname." to ".$newflow[0]->statusname." for task title ".$olddata[0]->tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update work flow status for ".$tasktitle." in ".$projectname,
                        'changestatus'=>$newflow,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update work flow status for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               if($olddata[0]->duedate != $dueto)
               {
                  
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update duedate ".$olddata[0]->duedate." to ".$dueto." for task title ".$olddata[0]->tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update duedate for ".$tasktitle." in ".$projectname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update duedate for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               if($olddata[0]->tags != $tags)
               {
                  
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update tags ".$olddata[0]->tags." to ".$tags." for task title ".$olddata[0]->tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update tags ".$tasktitle." in ".$projectname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update tags for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               if($olddata[0]->description != $description)
               {
                  
                    $record = array(
                        'taskid'=> $updateid,
                        'projectid'=> $projectid,
                        'userid'=> $created_by,
                        'action'=> "Update",
                        'username'=> $userdata[0]->name,
                        'useremail'=> $userdata[0]->email,
                        'description'=> $userdata[0]->name." update description ".$olddata[0]->description." to ".$description." for task title ".$olddata[0]->tasktitle." in ".$projectname,
                        'short_description'=> $userdata[0]->name." update description ".$tasktitle." in ".$projectname,
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
                   insertintotablewithlastid("tasklogs",$record);
				   $updatecomment = $userdata[0]->name." update task description for task <b>".$olddata[0]->taskordernumber."-".$tasktitle."</b> in <b>".$projectname."</b> project";
				   $this->sendupdateTaskmailuser($created_by, $projectid, $updateid, $updatecomment);
               }
               
               $data = array(
                        'projectid'=> $projectid,
                        'projecttitle'=> $projectname,
                        'tasktitle'=> $tasktitle,
                        'priority'=> $priority,
                        'workflowstatus'=> $workflowstatus,
                        'duedate'=> $dueto,
                        'assignto'=> $assignto,
                        'tags'=> $tags,
                        'description'=> $description
                      );
            $updateproject = updatetablerow('projecttasks',array("id"=>$updateid),$data); 
            $imagesarray = json_decode($images);
            if(!empty($imagesarray)) {
            foreach($imagesarray as $imagedata){
                updatetablerow("projecttask_images",array("id"=>$imagedata),array("recognizetext" => $updateid));
            }
            }
            
            if(!empty($updateproject)){
                      $response = array(
                         "status"=>true,
                         "message"=>"Task successfully update",
                         "mode"=>"edit"
                      );
                     echo json_encode($response);
                     die();}
               
           }else{
           
           $data = array(
                    'projectid'=> $projectid,
                    'taskordernumber'=> create_order_number(),
                    'projecttitle'=> $projectname,
                    'tasktitle'=> $tasktitle,
                    'priority'=> $priority,
                    'workflowstatus'=> $workflowstatus,
                    'duedate'=> $dueto,
                    'assignto'=> $assignto,
                    'tags'=> $tags,
                    'description'=> $description,
                    'created_by'=>$created_by,
                    'created_at'=>date("Y/m/d H:i:s"),
                    'updated_at'=>date("Y/m/d H:i:s")
                  );
            $insertrecordid = insertintotablewithlastid('projecttasks',$data);
            $this->sendTaskmailuser($created_by, $projectid, $insertrecordid);
            
            $imagesarray = json_decode($images);
             if(!empty($imagesarray)) {
                foreach($imagesarray as $imagedata){
                     updatetablerow("projecttask_images",array("id"=>$imagedata),array("recognizetext" => $insertrecordid));
                }
              }
            
            if(!empty($insertrecordid)){
                
                   $response = array(
                         "status"=>true,
                         "message"=>"Task successfully insert",
                         "mode"=>"insert"
                      );
                     echo json_encode($response);
                     die();}
           }
        } 
        
        
        function Taskimage(Request $request){
            if($files = $request->file('file'))
            {
                
                $image_name1 = array();
                if($_FILES && $files!=''){
                    foreach($files as $file){
                      $image_name = time() . '-' . $file->getClientOriginalName();
                      $ext = explode('.', $image_name);
                       $image_ext = array('jpeg','png','jpg','pdf','docx','xlsx');
                      if($file && !in_array($ext[1],$image_ext)){
                            return redirect($redirectUrl)->with("error", "Invalid image");
                      }
    
                       if($file && in_array($ext[1],$image_ext)){
                           $file->move( public_path() . '/product_image',$image_name);
                           $image_name1[]=$image_name;
                      }
                }
                
                }  
            }
            
            if(!empty($image_name1))
            {
                foreach($image_name1 as $image_n)
                {
                    $Rt = 'product_image/'.$image_n;
                    $data = array(
                           'image'=>$Rt,
                           'created_at'=>date("Y/m/d H:i:s"),
                           'updated_at'=>date("Y/m/d H:i:s")
                        );
                   $lastid = insertintotablewithlastid('projecttask_images',$data);
                   echo $lastid;
                }
            } 
        }
    
    function Taskcommentsave(Request $request)
    {
           $commentdata =  $request->get('commentdata');
           $projectid =  $request->get('commentprojectid');
           $taskid =  $request->get('commenttaskid');
           $userid =  $request->get('userid');
           $commenttasktitle =  $request->get('commenttasktitle');  
           $currentstatus = $request->get('currentstatus');
           $updatestatus = $request->get('updatestatus');
           $projectname = $request->get('projectname');
           
           if($currentstatus == $updatestatus){
              $record = array("status"=>false, "msg"=>"please check the status");
              echo json_encode($record); 
              die();
           }
           
           if(empty($commentdata) && empty($updatestatus))
           {
                $record = array("status"=>false, "msg"=>"please fill all the field");
                echo json_encode($record);
                 die();
           }
           $updateorderall =  updatetablerow("projecttasks",array("taskstatus"=>1),array("taskorderby"=>0));
           $updateorderone =  updatetablerow("projecttasks",array("id"=>$taskid),array("taskorderby"=>1));
           $userdata = gettablerow("users",array("id" => $userid));
		   $taskdataget = gettablerow("projecttasks",array("id"=>$taskid));
           if(!empty($commentdata))
           {
             $data = array(
               "user_id" => $userid,
               "projectid" => $projectid, 
               "taskid" => $taskid,
               "comments" => $commentdata,
               "created_at"=>date("Y/m/d H:i:s"),
               "updated_at"=>date("Y/m/d H:i:s")
               );
              $commentid =  insertintotablewithlastid("project_taskcomments",$data);  
              
              $changedata = $userdata[0]->name." commented on task <b>".$taskdataget[0]->taskordernumber."-".$commenttasktitle ."</b> in <b>".$projectname." </b> project.";
               
              $this->sendTaskcommentmail($projectid, $taskid, $changedata, $commentdata);
           }
           
           if(!empty($updatestatus)){
                   $updatedstatus =  updatetablerow("projecttasks",array("id"=> $taskid),array("workflowstatus"=>$updatestatus));
                   
                 $cursta =   gettablerow("workflowstatuss",array("id"=>$currentstatus));
                 $updsta =   gettablerow("workflowstatuss",array("id"=>$updatestatus));
                
                 $cur = $cursta[0]->statusname;
                 $upd = $updsta[0]->statusname;
                 $commentdatasta = $cur." status changed to ".$upd;
                 
                      $updatedata = array(
                           "user_id" => $userid,
                           "projectid" => $projectid,
                           "taskid" => $taskid,
                           "comments" => $commentdatasta,
                           "created_at"=>date("Y/m/d H:i:s"),
                           "updated_at"=>date("Y/m/d H:i:s")
                           );
                       $commentid =  insertintotablewithlastid("project_taskcomments",$updatedata);
                       
                       $changedata = $userdata[0]->name." change the task status for <b>".$taskdataget[0]->taskordernumber."-".$commenttasktitle ."</b> in <b>".$projectname."</b> project";
                       $this->sendTaskcommentmail($projectid, $taskid, $changedata, $commentdatasta);
                   
             }
               $allcomment = gettablerow("project_taskcomments",array("taskid"=>$taskid));
                $html = "";
                foreach($allcomment as $allcom){
                    $udet = getuserdetail($allcom->user_id);
                    $date=date_create($udet[0]->created_at);
                    $pdate =  date_format($date,"m/d/Y");
                    $first_char = mb_substr($udet[0]->name, 0, 1);
                    $html .= "<div id='e2281051' class='chronology__item chronology__item--action'> <div class='chronology-action'> <div class='chronology-action__avatar'> <div title='Mark Gorman- Spirit Worx' class='avatar avatar--light-orange' style='width: 40px; height: 40px; border-radius: 50%;'><span class='avtar-first-letter'>".$first_char."</span> </div> </div> <div class='chronology-action__date'>".$pdate."</div> <div class='chronology-action__action-text'><strong>".$udet[0]->name."</strong> ".$allcom->comments." </div></div></div>";
                }
                
                $record = array("status"=>true, "html"=>$html);
                echo json_encode($record);
       }
    function Deleteproject(Request $request){
       $deleteId =  $request->get('deleteId');
       $deleteTable =  $request->get('deleteTable');
       $projectdelete = deletetablerow($deleteTable,array('id'=>$deleteId));
       $projecttaskdelete = deletetablerow("projecttasks",array('projectid'=>$deleteId));
       if($projectdelete)
        {
             $record = array('status'=>true,'msg'=>'Deleted Successfully');
        }else{
             $record = array('status'=>false,'msg'=>'Not Deleted Successfully');
        }
        echo json_encode($record);
      
    }    
    function Tasklist($project = Null,$status = Null){
        $user = Auth::user()->id;
        if($user == 1){
            $alltask = getAllTaskList($project,$status);
           // $allassign = gettablerowgroupby("projecttasks",array("taskstatus"=>1),"assignto");
            $allassign = array();
            $getsearchstatus = getsearchstatus();
            $allproject = gettablerow("projects",array("projectstatus"=>1));
            $allstatus = gettablerow("workflowstatuss",array("status"=>1));
            $taskproject = $project;
            if(!empty($project)){
                $filter = "notshow";
            }else{
                $filter = "show";
            }
        }else{
            $alltask = getAllTaskListbyuser($project,$status,$user);
           // $allassign = gettablerowgroupbyuser("projecttasks",array("taskstatus"=>1),"assignto",$user);
            $allassign = array();
            $getsearchstatus = getsearchstatus($user);
            $allproject = getprojectbyuserid("projects",$user); 
            $allstatus = gettablerow("workflowstatuss",array("status"=>1));
            $taskproject = $project;
            if(!empty($project)){
                $filter = "notshow";
            }else{
                $filter = "show";
            }
        }
       return view('project.tasklist', compact(['alltask','allproject','allassign','getsearchstatus','allstatus','filter','taskproject','user']));    
    }
    
    function Getfilterdata(Request $request)
    {
         $projecttakevalue =  $request->get('projecttakevalue');
         $filtertitle =  $request->get('filtertitle');
         $filterassign =  $request->get('filterassign');
         $sortby =  $request->get('sortby'); 
         $selectfilterstatus =  $request->get('selectfilterstatus'); 
         $selectfilterpriority =  $request->get('selectfilterpriority'); 
         $user = Auth::user()->id;
         if($user == 1){
            $list = getfiltertaskhelper($projecttakevalue,$filtertitle,$filterassign,$sortby,$selectfilterstatus,$selectfilterpriority);  
         }else{
              $list = getfiltertaskhelperuser($projecttakevalue,$filtertitle,$filterassign,$sortby,$user,$selectfilterstatus,$selectfilterpriority);
         }
        
        
         $html = "";
         $html .= "<table id='datatable' class='table table-bordered dt-responsive nowrap' style=' width: 100%;'> <thead> <tr>  <th  colspan='6'>Task</th>  </tr> </thead> <tbody>";
         foreach($list as $allta){
         $link = route('manage-task',[base64_encode($allta->projectid),base64_encode($allta->id)]);
         $html .="<tr> <td class='col-status'><a href=".$link."><span class='badge bg-warning'  style='background-color: ".$allta->color."!important'>".$allta->status_name."</span><a></td>";  
        
         $html .="<td class='col--item-info three-lines' colspan='3'><div class='line line-1'><a href=".$link." class='just-a-link'>".$allta->taskordernumber." ".$allta->project_name."</a></div>";
        
         $html .="<a href=".$link."><div class='line line-1'><span class='item-title'>".$allta->tasktitle."</span></div> <div class='line line-2'><span class='item-desc'>".substr($allta->description,0,150)."</span></div></a> </a> </td>";
         $html .="<td style='width:15%;'><span title='Thread count'>";
         if($allta->commentscounts > 0){
         $html .="<i class='uil-comment'></i><span>".$allta->commentscounts."</span>";
         }
         $html .="</span>";
         if($allta->priority == "High") {
             $img = asset('/images/high.svg');
             $html .=" <div title='High' class='chicklet--small' style='background-color: #fa6123;  border: 1px solid #fa6123;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
          if($allta->priority == "Medium") {
             $img = asset('/images/medium.svg');
             $html .=" <div title='Medium' class='chicklet--small' style='background-color: #faa33e; border: 1px solid #faa33e;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
          if($allta->priority == "Low") {
             $img = asset('/images/low.svg');
             $html .=" <div title='Low' class='chicklet--small' style='background-color: #198c62; border: 1px solid #198c62;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
         $html .="</td>";
         $date = date_create($allta->created_at);
         $html .="<td class='col-last-updated'>".date_format($date,"m/d/Y")."</td>  </tr>";
        }
        $html .=" </tbody> </table>";
        $status ="";
        
       $allstatus = gettablerow("workflowstatuss",array("status"=>1));
        
       $html2 = ""; 
         $pre = array();
       foreach($allstatus as $stat){
         foreach($list as $allta1) { 
              if($allta1->workflowstatus == $stat->id)
             {
                $pre[] = $allta1->workflowstatus;
             }
         }  
       }
       
       foreach($allstatus as $workr){
       if(in_array($workr->id, $pre)){ 
        // $taskby = getAllTaskListbystatusfilter($workr->id,base64_encode($projecttakevalue));
        if($selectfilterstatus !="")
        {
            $filstas = $selectfilterstatus;
        }else{
             $filstas = $workr->id;
        }
         if($user == 1){
            $taskby = getfiltertaskhelper($projecttakevalue,$filtertitle,$filterassign,$sortby,$filstas,$selectfilterpriority);  
         }else{
              $taskby = getfiltertaskhelperuser($projecttakevalue,$filtertitle,$filterassign,$sortby,$user,$filstas,$selectfilterpriority);
         }
         //$taskby = getfiltertaskhelper($projecttakevalue,$filtertitle,$filterassign,$sortby,$user,$selectfilterstatus,$selectfilterpriority);
         //echo "<pre>"; print_r($taskby); echo "</pre>";
         $html2 .="<div class='kanban-column'> <div class='card kanvan-view-list-container' > <div class='card-body'>";
         $rsta = gettablerow("workflowstatuss",array("id"=>$workr->id));
         $html2 .= "<h4 class='card-title mb-4 bg-primary' style='background-color:".$rsta[0]->color."!important;'>".$rsta[0]->statusname."</h4>";
         $html2 .="<div class='kanban-column__card-holder'><ul>";
         foreach($taskby as $task){
             $linkk = route('manage-task',[base64_encode($task->projectid),base64_encode($task->id)]);
             $html2 .= "<li class=''><a href=".$linkk."><p>".$task->project_name."</p> <h4>".$task->tasktitle."</h4></a><div>";
             if($task->priority == "High") {
                 $html2 .= "<div title='High' style='background-color: #fa6123;  border: 1px solid #fa6123;' class='chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase'><span class='chicklet__content'><span>";
                 $srck = asset("/images/high.svg");
                 $html2 .="<img src=".$srck." <span class='with-icon'>".$task->priority."</span></span> </span> </div>";
             }
             if($task->priority == "Medium") {
                 $html2 .= "<div title='Medium' style='background-color: #faa33e; border: 1px solid #faa33e;' class='chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase'><span class='chicklet__content'><span>";
                 $srck = asset("/images/medium.svg");
                 $html2 .="<img src=".$srck." <span class='with-icon'>".$task->priority."</span></span> </span> </div>";
             }
             if($task->priority == "Low") {
                 $html2 .= "<div title='Low' style='background-color: #198c62; border: 1px solid #198c62;' class='chicklet priority-chicklet bg-reddish-orange-flat chicklet--flat chicklet--tiny bg-reddish-orange-flat--not-hover chicklet--uppercase'><span class='chicklet__content'><span>";
                 $srck = asset("/images/low.svg");
                 $html2 .="<img src=".$srck." <span class='with-icon'>".$task->priority."</span></span> </span> </div>";
             }
             
             $html2 .="</div> </li>";
        }
        
        $html2 .="</ul> </div> </div> </div> </div>";
           
       }
           
       }
        $rec = array("status"=>true,"html"=>$html,"html2"=>$html2);
        echo json_encode($rec);
    }
    
    
     function Getreportdata(Request $request)
    {
         $selectproject =  $request->get('selectproject');
         $selectstatus =  $request->get('selectstatus');
         $selectfrom =  $request->get('selectfrom');
         $selectto =  $request->get('selectto'); 
        
         $user = Auth::user()->id;
         if($user == 1){
            $list = getreporttaskhelper($selectproject,$selectstatus,$selectfrom,$selectto);  
         }else{
              $list = getreporttaskhelperuser($selectproject,$selectstatus,$selectfrom,$selectto,$user);
         }
		 
		 $html = "";
         $html .= "<table id='datatable' class='table table-bordered dt-responsive nowrap' style=' width: 100%;'> <thead> <tr>  <th  colspan='6'>Task</th>  </tr> </thead> <tbody>";
         foreach($list as $allta){
         $link = route('manage-task',[base64_encode($allta->projectid),base64_encode($allta->id)]);
         $html .="<tr> <td class='col-status'><a href=".$link."><span class='badge bg-warning'  style='background-color: ".$allta->color."!important'>".$allta->status_name."</span><a></td>";  
        
         $html .="<td class='col--item-info three-lines' colspan='3'><div class='line line-1'><a href=".$link." class='just-a-link'>".$allta->taskordernumber." ".$allta->project_name."</a></div>";
        
         $html .="<a href=".$link."><div class='line line-1'><span class='item-title'>".$allta->tasktitle."</span></div> <div class='line line-2'><span title='".$allta->description."' class='item-desc'>".$allta->description."</span></div></a> </a> </td>";
         $html .="<td style='width:15%;'>";
         
         if($allta->priority == "High") {
             $img = asset('/images/high.svg');
             $html .=" <div title='High' class='chicklet--small' style='background-color: #fa6123;  border: 1px solid #fa6123;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
          if($allta->priority == "Medium") {
             $img = asset('/images/medium.svg');
             $html .=" <div title='Medium' class='chicklet--small' style='background-color: #faa33e; border: 1px solid #faa33e;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
          if($allta->priority == "Low") {
             $img = asset('/images/low.svg');
             $html .=" <div title='Low' class='chicklet--small' style='background-color: #198c62; border: 1px solid #198c62;'>  <span class='chicklet__content'><img src= ".$img." ></span> </div>";
         }
         $html .="</td>";
         $date = date_create($allta->created_at);
         $html .="<td class='col-last-updated'>".date_format($date,"m/d/Y")."</td>  </tr>";
        }
        $html .=" </tbody> </table>";
		
		$rec = array("status"=>true,"html"=>$html);
        echo json_encode($rec);
        
    }
    
    
    function Saveproject(Request $request)
    {
         $projectname =  $request->get('projectname');
         $companyname =  $request->get('companyname');
         $workflowname =  $request->get('workflowname');
         $invtes = $request->get('invite');
         $invite =  implode(",",$invtes);
         $usertype1 =  (!empty($request->get('usertype1')))?$request->get('usertype1'):NULL;
         $usertype2 =  (!empty($request->get('usertype2')))?$request->get('usertype2'):NULL;
         $usertype3 =  (!empty($request->get('usertype3')))?$request->get('usertype3'):NULL;
         $usertype4 =  (!empty($request->get('usertype4')))?$request->get('usertype4'):NULL;
         $usertype5 =  (!empty($request->get('usertype5')))?$request->get('usertype5'):NULL;
         $usertype6 =  (!empty($request->get('usertype6')))?$request->get('usertype6'):NULL;
         $usertype7 =  (!empty($request->get('usertype7')))?$request->get('usertype7'):NULL;
         $usertype8 =  (!empty($request->get('usertype8')))?$request->get('usertype8'):NULL;
         $usertype9 =  (!empty($request->get('usertype9')))?$request->get('usertype9'):NULL;
         $usertype10 = (!empty($request->get('usertype10')))?$request->get('usertype10'):NULL;
         $updateid = $request->get('projectid');
         $createdby = Auth::user()->id;
         if(empty($projectname) || empty($companyname))
         {
             $response = array(
                 "status"=>false,
                 "message"=>"Please fill the data properly"
             );
             echo json_encode($response);
             die();
         }
         $checkinvite = $request->get('invite');
        
         
         $checkin = array();
        
         foreach($checkinvite as $check)
         {
            if($check != ""){
             $getuser =  gettablerow("users",array("email"=>$check));
            if(sizeof($getuser) == 0){
               $data = array(
                    'name' => "invite user",
                    'email' => $check,
                    'password' => Hash::make("Studio@123QWERTY"), 
                    'usertype' => '2',
                    'created_at' => date("Y/m/d H:i:s"),
                    'updated_at' => date("Y/m/d H:i:s")
                    );
               $userid =  insertintotablewithlastid("users",$data);
               $checkin[] = $userid;
               $test = Str::random(64);
               $record = array(
                    'email' => $check, 
                    'token' => Hash::make($test),
                    'created_at' => Carbon::now()
                   );
                   
               $resetid =  insertintotablewithlastid("password_resets",$record);
               $this->sendResetEmailtoinviteuser($check, $test);
             }else{
                $checkin[] = $getuser[0]->id; 
             }
           }else{
                $checkin[] = "!";
           }
        }
         $invit = implode(",",$checkin);
       
      
         $notify1 =  $request->get('notify1');
         if($notify1 != ""){
             $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify1 = preg_replace($pattern, $replacement, $notify1);
             $getnotify =  gettablerow("users",array("email"=>$notify1));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify1,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti1 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify1, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify1, $test);
               }else{
                   $noti1 = $getnotify[0]->id; 
                   $noti1email = $getnotify[0]->email; 
                   $noti1name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                   $this->sendNotifytointernaluser($noti1email, $noti1name, $projectname);
               } 
         }else{
              $noti1 = NULL; 
         }
        
       
         $notify2 =  $request->get('notify2');
         if($notify2 != "")
         {
             $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify2 = preg_replace($pattern, $replacement, $notify2);
            $getnotify =  gettablerow("users",array("email"=>$notify2));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify2,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti2 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify2, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify2, $test);
               }else{
                  $noti2 = $getnotify[0]->id; 
                  $noti2email = $getnotify[0]->email; 
                  $noti2name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti2email, $noti2name, $projectname);
               } 
         }else{
              $noti2 = NULL; 
         }
         
         $notify3 =  $request->get('notify3');
         if($notify3 != "")
         {
              $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify3 = preg_replace($pattern, $replacement, $notify3);
            $getnotify =  gettablerow("users",array("email"=>$notify3));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify3,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti3 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify3, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify3, $test);
               }else{
                   $noti3 = $getnotify[0]->id;
                   $noti3email = $getnotify[0]->email; 
                   $noti3name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                   $this->sendNotifytointernaluser($noti3email, $noti3name, $projectname);
               } 
         }else{
              $noti3 = NULL; 
         }
         
         $notify4 =  $request->get('notify4');
         if($notify4 != "")
         {
              $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify4 = preg_replace($pattern, $replacement, $notify4);
            $getnotify =  gettablerow("users",array("email"=>$notify4));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify4,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti4 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify4, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify4, $test);
               }else{
                  $noti4 = $getnotify[0]->id;
                   $noti4email = $getnotify[0]->email; 
                $noti4name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti4email, $noti4name, $projectname);
               } 
         }else{
              $noti4 = NULL; 
         }
         
         $notify5 =  $request->get('notify5');
         if($notify5 != "")
         {
              $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify5 = preg_replace($pattern, $replacement, $notify5);
            $getnotify =  gettablerow("users",array("email"=>$notify5));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify5,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti5 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify5, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify5, $test);
               }else{
                  $noti5 = $getnotify[0]->id; 
                    $noti5email = $getnotify[0]->email; 
                $noti5name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti5email, $noti5name,$projectname);
               } 
         }else{
              $noti5 = NULL;  
         }
         
         $notify6 =  $request->get('notify6');
         if($notify6 != "")
         {
             $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify6 = preg_replace($pattern, $replacement, $notify6);
             $getnotify =  gettablerow("users",array("email"=>$notify6));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify6,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti6 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify6, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify6, $test);
               }else{
                  $noti6 = $getnotify[0]->id; 
                   $noti6email = $getnotify[0]->email; 
                $noti6name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti6email, $noti6name, $projectname);
               } 
         }else{
              $noti6 = NULL; 
         }
         
         $notify7 =  $request->get('notify7');
         if($notify7 != "")
         {
               $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify7 = preg_replace($pattern, $replacement, $notify7);
            $getnotify =  gettablerow("users",array("email"=>$notify7));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify7,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti7 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify7, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify7, $test);
               }else{
                  $noti7 = $getnotify[0]->id; 
                   $noti7email = $getnotify[0]->email; 
                $noti7name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti7email, $noti7name, $projectname);
               } 
         }else{
              $noti7 = NULL; 
         }
         
         $notify8 =  $request->get('notify8');
         if($notify8 != "")
         {
                $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify8 = preg_replace($pattern, $replacement, $notify8);
            $getnotify =  gettablerow("users",array("email"=>$notify8));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify8,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti8 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify8, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify8, $test);
               }else{
                  $noti8 = $getnotify[0]->id; 
                    $noti8email = $getnotify[0]->email; 
                $noti8name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti8email, $noti8name, $projectname);
               } 
         }else{
              $noti8 = NULL; 
         }
         
         $notify9 =  $request->get('notify9');
         if($notify9 != "")
         {
                  $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify9 = preg_replace($pattern, $replacement, $notify9);
            $getnotify =  gettablerow("users",array("email"=>$notify9));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify9,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti9 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify9, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify9, $test);
               }else{
                  $noti9 = $getnotify[0]->id; 
                    $noti9email = $getnotify[0]->email; 
                $noti9name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti9email, $noti9name, $projectname);
               } 
         }else{
              $noti9 = NULL; 
         }
         
         $notify10 =  $request->get('notify10');
         if($notify10 != "")
         {
                   $pattern = '/(\(\w+\))/';
             $replacement = '';
             $notify10 = preg_replace($pattern, $replacement, $notify10);
            $getnotify =  gettablerow("users",array("email"=>$notify10));
             if(sizeof($getnotify) == 0){
                   $data = array(
                        'name' => "invite user",
                        'email' => $notify10,
                        'password' => Hash::make("Studio@123QWERTY"),
                        'usertype' => '3',
                        'created_at' => date("Y/m/d H:i:s"),
                        'updated_at' => date("Y/m/d H:i:s")
                        );
                   $userid =  insertintotablewithlastid("users",$data);
                   $noti10 = $userid;
                   $test = Str::random(64);
                   $record = array(
                        'email' => $notify10, 
                        'token' => Hash::make($test),
                        'created_at' => Carbon::now()
                       );
                       
                   $resetid =  insertintotablewithlastid("password_resets",$record);
                   $this->sendResetEmailtonotify($notify10, $test); 
               }else{
                  $noti10 = $getnotify[0]->id; 
                    $noti10email = $getnotify[0]->email; 
                $noti10name = ($getnotify[0]->name != "invite user")?$getnotify[0]->name:$getnotify[0]->email;
                  $this->sendNotifytointernaluser($noti10email, $noti10name, $projectname);
               } 
         }else{
              $noti10 = NULL; 
         }
         
        
         if(!empty($updateid))
         {
             $data = array(
                      'projectname'=> $projectname,
                      'companyname'=> $companyname,
                      'workflowname'=> $workflowname,
                       'inviteclientusers'=> $invit,
                       'notify1'=> $noti1,
                       'usertype1'=> $usertype1,
                       'notify2'=> $noti2,
                       'usertype2'=> $usertype2,
                        'notify3'=> $noti3,
                       'usertype3'=> $usertype3,
                       'notify4'=> $noti4,
                       'usertype4'=> $usertype4,
                        'notify5'=> $noti5,
                       'usertype5'=> $usertype5,
                       'notify6'=> $noti6,
                       'usertype6'=> $usertype6,
                        'notify7'=> $noti7,
                       'usertype7'=> $usertype7,
                       'notify8'=> $noti8,
                       'usertype8'=> $usertype8,
                        'notify9'=> $noti9,
                       'usertype9'=> $usertype9,
                       'notify10'=> $noti10,
                       'usertype10'=> $usertype10
                    );
             $updateproject = updatetablerow('projects',array("id"=>$updateid),$data);
             if($updateproject){
                  $response = array(
                     "status"=>true,
                     "message"=>"Project successfully updated"
                     );
                     echo json_encode($response);
                     die();
             }
         }else{
             $data = array(
                       'projectname'=> $projectname,
                       'companyname'=> $companyname,
                       'workflowname'=> $workflowname,
                       'inviteclientusers'=> $invit,
                        'notify1'=> $noti1,
                       'usertype1'=> $usertype1,
                       'notify2'=> $noti2,
                       'usertype2'=> $usertype2,
                        'notify3'=> $noti3,
                       'usertype3'=> $usertype3,
                       'notify4'=> $noti4,
                       'usertype4'=> $usertype4,
                        'notify5'=> $noti5,
                       'usertype5'=> $usertype5,
                       'notify6'=> $noti6,
                       'usertype6'=> $usertype6,
                        'notify7'=> $noti7,
                       'usertype7'=> $usertype7,
                       'notify8'=> $noti8,
                       'usertype8'=> $usertype8,
                        'notify9'=> $noti9,
                       'usertype9'=> $usertype9,
                       'notify10'=> $noti10,
                       'usertype10'=> $usertype10,
                       'created_by'=>$createdby,
                       'created_at'=>date("Y/m/d H:i:s"),
                       'updated_at'=>date("Y/m/d H:i:s")
                      );
             $insertrecord = insertintotable('projects',$data);
             if($insertrecord){
                  $response = array(
                     "status"=>true,
                     "message"=>"Project successfully insert"
                 );
                 echo json_encode($response);
                 die();
             }
         }
   }
   
    private function sendResetEmailtoinviteuser($email, $token)
    {
       
    //Generate, the password reset link. The token generated is embedded in the link
    $link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($email);
    $html = "Dear ".$email."<br/><br/>";
    $html .= "You are invited to Argos Help Desk.<br/><br/>";
    $html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> to set your password in the Argos Helpdesk tool and create an account.<br/><br/>";
    $html .= "Regards,<br/>";
    $html .= "Argos Help Desk";
    try {
         $details = [
        'title' => "You are invited to Argos Help Desk" ,
        'body' => $html
        ];
   
    \Mail::to($email)->send(new \App\Mail\MyTestMail($details));
       return true;
    } catch (\Exception $e) {
        return false;
    }
    }

 private function sendTaskcommentmail($projectid, $taskid, $changedata, $commentdata)
    {
       
    $projectdetail =  gettablerow("projects",array("id"=> $projectid));
    $taskdetail =  gettablerow("projecttasks",array("id"=> $taskid));

     for($i = 1; $i <=10; $i++){
         $notifyget = "notify".$i;
         if(!empty($projectdetail[0]->$notifyget))
            {
              $curid =  $projectdetail[0]->$notifyget; 
			  $user = Auth::user()->id;
			  if($curid != $user)
			  {
              $curuserdetail =  gettablerow("users",array("id"=> $curid));  

              $test = Str::random(64);
              $record = array(
                        'projectid' => $projectid,
                        'taskid'=> $taskid,
                        'userid'=> $curid,
                        'token' => Hash::make($test),
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
             $checkid = insertintotablewithlastid("tasktokens",$record);
              $link = config('base_url') . 'tasklink/' . $checkid . '/' . $test;

               
                $html = "Dear ".$curuserdetail[0]->name." <br/><br/>";
                $html .= $changedata."<br/><br/>";
                $html .= "Comments: ".$commentdata." <br/><br/>";
                $html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> for visit the task.<br/><br/>";
                $html .= "Regards,<br/>";
                $html .= "Argos Help Desk";
                $details = [
                    'title' => "Comment added in task" ,
                    'body' => $html
                    ];
               
                \Mail::to($curuserdetail[0]->email)->send(new \App\Mail\MyTestMail($details));
			  }
          }
       }
      
      $invite = explode(",",$projectdetail[0]->inviteclientusers);
      foreach($invite as $inv)
      {
        if($inv != "!")
        {
           $curid =  $inv; 
		   $user = Auth::user()->id;
		   if($curid != $user)
		   {
              $curuserdetail =  gettablerow("users",array("id"=> $curid));  

              $test = Str::random(64);
              $record = array(
                        'projectid' => $projectid,
                        'taskid'=> $taskid,
                        'userid'=> $curid,
                        'token' => Hash::make($test),
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
             $checkid = insertintotablewithlastid("tasktokens",$record);
              $link = config('base_url') . 'tasklink/' . $checkid . '/' . $test;

               
                $html = "Dear ".$curuserdetail[0]->name." <br/><br/>";
                $html .= $changedata.". <br/><br/>";
                $html .= "Comments: ".$commentdata.". <br/><br/>";
                $html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> for visit the task.<br/><br/>";
                $html .= "Regards,<br/>";
                $html .= "Argos Help Desk";
                $details = [
                    'title' => "Comment added in task" ,
                    'body' => $html
                    ];
               
                \Mail::to($curuserdetail[0]->email)->send(new \App\Mail\MyTestMail($details)); 
		   }
        }
      }

   }

    

    private function sendResetEmailtonotify($email, $token)
    {
       
    //Generate, the password reset link. The token generated is embedded in the link
    $link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($email);
    $html = "Dear ".$email."<br/><br/>";
    $html .= "You are invited to Argos Help Desk.<br/><br/>";
    $html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> to set your password in the Argos Helpdesk tool and create an account.<br/><br/>";
    $html .= "One project is assigned to you. Please login to your account and check the project task detail.<br/><br/>";
    $html .= "If any queries please contact to your Administrator.<br/>";
    $html .= "Regards,<br/>";
    $html .= "Argos Help Desk";
    try {
         $details = [
        'title' => "You are invited to Argos Help Desk" ,
        'body' => $html
        ];
   
    \Mail::to($email)->send(new \App\Mail\MyTestMail($details));
       return true;
    } catch (\Exception $e) {
        return false;
    }
    }

    private function sendNotifytointernaluser($email, $name, $projectname)
    {
    //Generate, the password reset link. The token generated is embedded in the link
    
    $html = "Dear ".$name." <br/><br/>";
    $html .= "One project <b>". $projectname ."</b> is assigned to you. Please <a href ='https://helpdesk.argosinfotech.net/login' style='text-decoration: underline;'> login </a> to your account and check the project task detail.<br/><br/>";
    $html .= "If any queries please contact to your Administrator.<br/>";
    $html .= "Regards,<br/>";
    $html .= "Argos Help Desk";
    try {
         $details = [
        'title' => "One project is assigned to you" ,
        'body' => $html
        ];
   
    \Mail::to($email)->send(new \App\Mail\MyTestMail($details));
       return true;
    } catch (\Exception $e) {
        return false;
    }
    }
    
	
	private function sendupdateTaskmailuser($created_by, $projectid, $taskid, $updatecomment)
    {
    $userdetail =  gettablerow("users",array("id"=> $created_by));
    $projectdetail =  gettablerow("projects",array("id"=> $projectid));
    $taskdetail =  gettablerow("projecttasks",array("id"=> $taskid));

    for($i = 1; $i <=10; $i++){
         $notifyget = "notify".$i;
         if(!empty($projectdetail[0]->$notifyget))
            {
               $curid =  $projectdetail[0]->$notifyget; 
			   $user = Auth::user()->id;
			   if($curid != $user){
               $curuserdetail =  gettablerow("users",array("id"=> $curid));
			   
			  $test = Str::random(64);
              $record = array(
                        'projectid' => $projectid,
                        'taskid'=> $taskid,
                        'userid'=> $curid,
                        'token' => Hash::make($test),
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
              $checkid = insertintotablewithlastid("tasktokens",$record);
              $link = config('base_url') . 'tasklink/' . $checkid . '/' . $test;
			   
                $html = "Dear ".$curuserdetail[0]->name." <br/><br/>";
                $html .= $updatecomment."<br/><br/>";
                $html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> to review the task.<br/><br/>";
                $html .= "If any queries please contact to your Administrator.<br/>";
                $html .= "Regards,<br/>";
                $html .= "Argos Help Desk";
                $details = [
                    'title' => "One task updated" ,
                    'body' => $html
                    ];
               
                \Mail::to($curuserdetail[0]->email)->send(new \App\Mail\MyTestMail($details));
			   }
          }
       }
   }
    private function sendTaskmailuser($created_by, $projectid, $taskid)
    {
    $userdetail =  gettablerow("users",array("id"=> $created_by));
    $projectdetail =  gettablerow("projects",array("id"=> $projectid));
    $taskdetail =  gettablerow("projecttasks",array("id"=> $taskid));

    for($i = 1; $i <=10; $i++){
         $notifyget = "notify".$i;
         if(!empty($projectdetail[0]->$notifyget))
            {
               $curid =  $projectdetail[0]->$notifyget; 
			   $user = Auth::user()->id;
			   if($curid != $user){
               $curuserdetail =  gettablerow("users",array("id"=> $curid));
			   
			  $test = Str::random(64);
              $record = array(
                        'projectid' => $projectid,
                        'taskid'=> $taskid,
                        'userid'=> $curid,
                        'token' => Hash::make($test),
                        'created_at'=>date("Y/m/d H:i:s"),
                        'updated_at'=>date("Y/m/d H:i:s")
                      );
              $checkid = insertintotablewithlastid("tasktokens",$record);
              $link = config('base_url') . 'tasklink/' . $checkid . '/' . $test;
			   
                $html = "Dear ".$curuserdetail[0]->name." <br/><br/>";
                $html .= $userdetail[0]->name." added a task in project <b>".$projectdetail[0]->projectname." </b><br/><br/>";
                $html .= "Task title: ".$taskdetail[0]->tasktitle." <br/><br/>";
                $html .= "Task priority: ".$taskdetail[0]->priority." <br/><br/>";
                $html .= "Task description: ".$taskdetail[0]->description." <br/><br/>";
				$html .= "Please <a href ='https://helpdesk.argosinfotech.net/".$link."' style='text-decoration: underline;'> Click Here </a> to review the task.<br/><br/>";
                $html .= "If any queries please contact to your Administrator.<br/>";
                $html .= "Regards,<br/>";
                $html .= "Argos Help Desk";
                $details = [
                    'title' => "One task added" ,
                    'body' => $html
                    ];
               
                \Mail::to($curuserdetail[0]->email)->send(new \App\Mail\MyTestMail($details));
			 }
          }
       }
   }
}
