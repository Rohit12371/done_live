<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;

class Tasklink extends Controller
{
     function Tasklink($id,$token){
        $existdata = gettablerow("tasktokens",array("id"=>$id));
        if(sizeof($existdata) > 0){
            $datbasetoken = $existdata[0]->token;
            if(Hash::check($token,$datbasetoken)) {

               if (Auth::loginUsingId($existdata[0]->userid))
               {
				$proid = $existdata[0]->projectid; 
                $taid = $existdata[0]->taskid;
                deletetablerow("tasktokens",array("id"=>$id));
                return redirect()->to('https://helpdesk.argosinfotech.net/manage-task'.'/'.base64_encode($proid).'/'.base64_encode($taid));
              
               }else{
                 echo "invalid";
               }
            }
        }else{
          echo "invalid";  
        }
        

    }
}
